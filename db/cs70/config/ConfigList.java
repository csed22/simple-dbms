package eg.edu.alexu.csd.oop.db.cs70.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import eg.edu.alexu.csd.oop.db.cs70.tools.files.FileReader;
import eg.edu.alexu.csd.oop.db.cs70.tools.files.FileWriter;

/**
 * reads / edits the config.ini file
 * @author Ahmed Waleed
 * @category Configurations
 */
public class ConfigList {

	private File configFile;

	private HashMap<String, Boolean> properties = new HashMap<String, Boolean>();
	
	protected ConfigList() {
	    configFile = new File("config.ini");
	    resetproperties();
	    
		try {
			if(!configFile.exists())
				updateConfigFile();
			else
				readConfigFile();
		} catch (IOException e) {}
		
		//JOptionPane.showMessageDialog(null, "properties:\n" + propertiesTable(), "the title", JOptionPane.PLAIN_MESSAGE);
	}
	
	public boolean checkProperty(String property) {
		
		if(properties.containsKey(property))
			if(properties.get(property).booleanValue())
				return true;
		
		return false;
	}
	
	public void setProperty(String property, boolean value) {
		if(properties.containsKey(property))
			properties.put(property, value);
		
		updateConfigFile();
	}
	
	public void updateConfigFile() {
		FileWriter recorder = new FileWriter(configFile.getName());
		String[] record = {propertiesTable()};
		recorder.write(record);	
	}
	
	private void readConfigFile() throws IOException {
		FileReader reader = new FileReader(configFile.getName(), "\t", 2);
		String[][] record = null;
		
		record = reader.read();
		
		setProperty(record[0][0].substring(0, record[0][0].length()-2), record[0][1].equals("true"));
		setProperty(record[1][0].substring(0, record[1][0].length()-2), record[1][1].equals("true"));
		setProperty(record[2][0].substring(0, record[2][0].length()-2), record[2][1].equals("true"));
		
		// for some reason doing it with a for loop causes a NullPointerException .. @ java.awt.EventDispatchThread.run(EventDispatchThread.java:82)
		/*for(int i=0; i<record.length; i++) {
			setProperty(record[i][0].substring(0, record[i][0].length()-2), record[i][1].equals("true"));
		}*/
	}
	
	@SuppressWarnings("rawtypes")
	private String propertiesTable() {
		
		String s = "";
		
		Iterator propertiesIterator = properties.entrySet().iterator();
		
		// https://www.geeksforgeeks.org/traverse-through-a-hashmap-in-java/
		while(propertiesIterator.hasNext())
			s += readHashMapEntry((Entry) propertiesIterator.next());
		
		return s;
	}
	
	@SuppressWarnings("rawtypes")
	private String readHashMapEntry(Entry mapElement) {
		
		String property = "";
		property = mapElement.getKey().toString() + " :\t" + mapElement.getValue().toString() + '\n';
		
		return property;
	}
	
	private void resetproperties() {
		properties.put("CLI Mode", false);
		properties.put("SQL Read", false);
		properties.put("No Interrupt", false);
	}

}
