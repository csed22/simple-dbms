package eg.edu.alexu.csd.oop.db.cs70.config;

import eg.edu.alexu.csd.oop.db.cs70.Main;

/**
 * knows the program location and preferences.
 * @author Ahmed Waleed
 * @category Configurations
 * @dPattern Singleton
 */
public class Configurations {
	
	public  ConfigList properties;
	private String packagePath;
	private String mainName;
	
    // static variable single_instance of type Singleton 
    private static Configurations single_instance = null; 
  
    // private constructor restricted to this class itself 
    private Configurations(Main mainClass) {
    	String mainPath = mainClass.getClass().getName();
		packagePath = extractPackagePath(mainPath);
		mainName = extractMainName(mainPath);
		properties = new ConfigList();
    }
  
    // static method to create instance of Singleton class 
    public static Configurations getInstance() { 
        if (single_instance == null) 
            single_instance = new Configurations(new Main()); 
  
        return single_instance; 
    } 
    
	public String identifyJarLocation() {
		
		String fileName = Main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
		
		String ifYouRunFromIDE = packagePath + "/" + mainName + ".jar";
        if (!fileName.contains(mainName)) {
        	fileName += ifYouRunFromIDE;
        }

		return fileName.replace("%20", " ");
	}
	
	public String identifyConfigLocation() {
		
		//find jar current location
		String recordLocation = identifyJarLocation();
		//replace Main.jar -> trackOpenedWindows.txt
		recordLocation = recordLocation.replace(mainName + ".jar", "config.ini");
		//replace \ with //
		recordLocation = recordLocation.replace("/", "\\\\");
		//annoying note: \\\\ means \\ in the new string which is a single \ in the path.
		
		return recordLocation;
		
	}
	
	private int identifyLastDotLocation(String s) {
		int lastDot = 0;
		
		for(int i=0; i<s.length(); i++)
			if(s.charAt(i)=='.')
				lastDot = i;
		
		return lastDot;
	}
	
	private String extractPackagePath(String s) {
		
		int lastDot = identifyLastDotLocation(s);
		String packageName = new String();
		
		for(int i=0; i<lastDot; i++)
				packageName += s.charAt(i);
		
		return packageName.replace('.', '/');
		
	}
	
	private String extractMainName(String s) {
		
		int lastDot = identifyLastDotLocation(s);
		String lastName = new String();
		
		for(int i=lastDot+1; i<s.length(); i++)
				lastName += s.charAt(i);
		
		return lastName;
		
	}
	
} 

// https://www.geeksforgeeks.org/singleton-class-java/