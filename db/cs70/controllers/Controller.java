package eg.edu.alexu.csd.oop.db.cs70.controllers;

/**
 * <<interface>>
 * only the classes implementing Controller can read / edit
 * the configurations to control the flow of the program.
 * @author Ahmed Waleed
 * @category Controller
 */
public interface Controller {

	public void allow();
	public void deny();
	public boolean isAllowedToOpen();
	
}
