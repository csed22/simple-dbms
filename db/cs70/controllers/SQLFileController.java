package eg.edu.alexu.csd.oop.db.cs70.controllers;

/**
 * place holder, does not do any thing yet
 * but it is intended to change the main behavior,
 * when the GUI calls the CLI to execute .sql file.
 * @author Ahmed Waleed
 * @category Branching Flag
 */
public class SQLFileController implements Controller {

	@Override
	public void allow() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void deny() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isAllowedToOpen() {
		// TODO Auto-generated method stub
		return false;
	}

}
