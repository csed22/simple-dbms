package eg.edu.alexu.csd.oop.db.cs70.controllers;

import java.io.File;
import java.util.Scanner;


import eg.edu.alexu.csd.oop.db.cs70.config.Configurations;

/**
 * Since the main is responsible for opening it self,
 * stopping the infinite loop that may occur is a must.
 * @author Ahmed Waleed
 * @category InfiniteLoopPreventer
 */
public class InterfaceController implements Controller{
	
	public void allow() {
		Configurations.getInstance().properties.setProperty("CLI Mode", false);
	}
	
	public void deny() {
		Configurations.getInstance().properties.setProperty("CLI Mode", true);
	}
	
	/**
	 * Checks if the program is already opened from the same .jar using config.ini file
	 */
	public boolean isAllowedToOpen() {
		
		if(firstExcution())   
			return true;
		if(isCLIModeON()) 
			return false;	
		
		return true;
		
	}
	
	private boolean firstExcution() {
		Scanner input = null;
		
		try {
			input = new Scanner(new File(Configurations.getInstance().identifyConfigLocation()));
		}catch(Exception e){}
		
		return input == null;
	}
	
	private boolean isCLIModeON() {
		return Configurations.getInstance().properties.checkProperty("CLI Mode");
	}

}
