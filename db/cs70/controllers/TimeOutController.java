package eg.edu.alexu.csd.oop.db.cs70.controllers;

import eg.edu.alexu.csd.oop.db.cs70.config.Configurations;

/**
 * easiest way to control the timeout interrupter,
 * it won't be allowed to enter an infinite loop.
 * @author Ahmed Waleed
 * @category InfiniteLoopPreventer
 */
public class TimeOutController implements Controller{

	public void allow() {
		Configurations.getInstance().properties.setProperty("No Interrupt", false);
	}
	
	public void deny() {
		Configurations.getInstance().properties.setProperty("No Interrupt", true);
	}

	public boolean isAllowedToOpen() {
		return !(Configurations.getInstance().properties.checkProperty("No Interrupt"));
	}

}
