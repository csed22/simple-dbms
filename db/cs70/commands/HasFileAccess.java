package eg.edu.alexu.csd.oop.db.cs70.commands;

import java.util.ArrayList;
import java.util.HashMap;

import eg.edu.alexu.csd.oop.db.cs70.tools.xml.XMLFileManager;
import eg.edu.alexu.csd.oop.db.cs70.validators.HierarchyReader;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;

public abstract class HasFileAccess {

	protected void saveCurrentTable(String db, String t, ArrayList<HashMap<String, Object>> currentTable) {
		HierarchyReader info = new HierarchyReader();
		DBNode database = info.fetchDatabase(db);
		//System.out.println("Trying to delete: " + database.getDbPath() + "\\"  + t + ".xml" );
		new XMLFileManager().setTableData(database.getDbPath(), t, currentTable);
	}

	protected ArrayList<HashMap<String, Object>> loadCurrentTable(String db, String t) {
		HierarchyReader info = new HierarchyReader();
		DBNode database = info.fetchDatabase(db);
		//System.out.println("Trying to load: " + database.getDbPath() + "\\"  + t + ".xml" );
		return new XMLFileManager().fetchTableData(database.getDbPath(), t);
	}
	
}
