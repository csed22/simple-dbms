package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

public class DropAlterCommand implements ICommand {

    private String[] groups;

    public DropAlterCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute(){
        FileManager fileManager = new XMLFileManager();
        ParametersValidator validator = new ParametersValidator();
        if(validator.isColumnHere(Facade.currentDatabaseName, this.groups[1].toLowerCase(), this.groups[2].toLowerCase())) {
            fileManager.dropColumn(Facade.currentDatabaseName, this.groups[1].toLowerCase(), this.groups[2].toLowerCase());
            return 1;
        }
        return 0;
    }
}
