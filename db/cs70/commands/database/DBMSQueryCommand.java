package eg.edu.alexu.csd.oop.db.cs70.commands.database;

import eg.edu.alexu.csd.oop.db.Database;

import java.sql.SQLException;

public class DBMSQueryCommand extends DBMSCommand{

    public DBMSQueryCommand(Database dbms, String input) {
        super(dbms, input);
    }

    @Override
    public Object execute() throws SQLException {
        Object[][] queryResult = this.dbms.executeQuery(this.input);
        return queryResult;
    }
}
