package eg.edu.alexu.csd.oop.db.cs70.commands.database;

import eg.edu.alexu.csd.oop.db.Database;
import eg.edu.alexu.csd.oop.db.cs70.commands.ICommand;

import java.sql.SQLException;

abstract class DBMSCommand implements ICommand {

    protected Database dbms;
    protected String input;

    public DBMSCommand(Database dbms, String input) {
        this.dbms = dbms;
        this.input = input;
    }

    @Override
    public abstract Object execute() throws SQLException;
}
