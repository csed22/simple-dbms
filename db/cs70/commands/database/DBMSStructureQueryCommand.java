package eg.edu.alexu.csd.oop.db.cs70.commands.database;

import eg.edu.alexu.csd.oop.db.Database;

import java.sql.SQLException;

public class DBMSStructureQueryCommand extends DBMSCommand {

    public DBMSStructureQueryCommand(Database dbms, String input) {
        super(dbms, input);
    }

    @Override
    public Object execute() throws SQLException {
        boolean structureQuerySuccess = this.dbms.executeStructureQuery(this.input);
        return structureQuerySuccess;
    }
}
