package eg.edu.alexu.csd.oop.db.cs70.commands.database;

import eg.edu.alexu.csd.oop.db.Database;

import java.sql.SQLException;

public class DBMSUpdateQueryCommand extends DBMSCommand {

    public DBMSUpdateQueryCommand(Database dbms, String input) {
        super(dbms, input);
    }

    @Override
    public Object execute() throws SQLException {
        int updatedRowsCounts = this.dbms.executeUpdateQuery(this.input);
        return updatedRowsCounts;
    }
}
