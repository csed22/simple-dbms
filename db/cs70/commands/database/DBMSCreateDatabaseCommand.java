package eg.edu.alexu.csd.oop.db.cs70.commands.database;

import eg.edu.alexu.csd.oop.db.Database;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

import java.sql.SQLException;

public class DBMSCreateDatabaseCommand extends DBMSCommand {

    private ParametersValidator databaseExistenceChecker = new ParametersValidator();
    private boolean dropIfExists;

    public DBMSCreateDatabaseCommand(Database dbms, String path) {
        super(dbms, path);
        String [] pathParts = path.split("\\\\|\\/");
        this.dropIfExists = databaseExistenceChecker.isDatabaseHere(pathParts[pathParts.length-1]);
    }

    @Override
    public Object execute() throws SQLException {
        String pathToDatabase = this.dbms.createDatabase(input, dropIfExists);
        return pathToDatabase;
    }
}
