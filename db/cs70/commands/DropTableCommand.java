package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;

public class DropTableCommand implements ICommand {
    private String[] groups;

    public DropTableCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        FileManager fileManager = new XMLFileManager();
        return fileManager.dropTable(Facade.currentDatabaseName, groups[1].toLowerCase());
    }
}
