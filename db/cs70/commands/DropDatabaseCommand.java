package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;

public class DropDatabaseCommand implements ICommand {

    private String [] groups;

    public DropDatabaseCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        String [] path = groups[1].split("\\\\|\\/");
        FileManager fileManager = new XMLFileManager();
        return fileManager.dropDatabase(path[path.length-1].toLowerCase());
    }
}
