package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;

public class CreateDatabaseCommand implements ICommand {

    private String [] groups;

    public CreateDatabaseCommand(String [] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        String [] path = groups[1].split("\\\\|\\/");
        //System.out.println("the path is: " + groups[1]);
        FileManager fileManager = new XMLFileManager();
        Facade.currentDatabaseName = path[path.length-1].toLowerCase();
        return fileManager.addDatabase(groups[1] /*Facade.currentDatabaseName*/);
    }
}
