package eg.edu.alexu.csd.oop.db.cs70.commands;

import java.sql.SQLException;

public interface ICommand {
    Object execute() throws SQLException;
}
