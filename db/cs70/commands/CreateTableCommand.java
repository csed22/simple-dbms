package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;

import java.sql.SQLException;

public class CreateTableCommand implements ICommand {

    private String[] groups;
    private FileManager fileManager = new XMLFileManager();

    public CreateTableCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() throws SQLException {
        if(Facade.currentDatabaseName == null)
            throw new SQLException();
        boolean returnValue = fileManager.addTable(Facade.currentDatabaseName, groups[1].toLowerCase());
        if (returnValue)
            initializeColumns(groups[2].replaceAll("\\s+", " "));
        return returnValue;
    }

    private void initializeColumns(String columns) {
        String[] columnDefinitions = columns.split(",");
        for (String columnDefinition : columnDefinitions) {
            String columnName = columnDefinition.replaceAll("^\\s", "").split("\\s")[0].toLowerCase();
            String columnType = columnDefinition.replaceAll("^\\s", "").split("\\s")[1].toLowerCase();
            try {
                switch (columnType) {
                    case "varchar":
                        fileManager.addColumn(Facade.currentDatabaseName, groups[1].toLowerCase(), columnName, String.class);
                        break;
                    case "int":
                        fileManager.addColumn(Facade.currentDatabaseName, groups[1].toLowerCase(), columnName, Integer.class);
                        break;
                    default:
                        throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("Invalid Data Type");
            }
        }
    }
}