package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;
import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;

import java.util.HashMap;

public class InsertIntoCommand extends HasFileAccess implements ICommand {

    private String [] groups;

    public InsertIntoCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        FileManager fileManager = new XMLFileManager();
        HashMap<String, Object> row = fillRow();
        if(row != null) {
            fileManager.addRow(Facade.currentDatabaseName, this.groups[1].toLowerCase(), row);
            return 1;
        }
        return 0;
    }

    private HashMap <String, Object> fillRow(){
        try {
            HashMap<String, Object> row = new HashMap<>();
            String[] columnNames;
            if (this.groups[2] == null)
                columnNames = loadCurrentTable(Facade.currentDatabaseName, this.groups[1].toLowerCase()).get(0).keySet().toArray(new String[0]);
            else
                columnNames = groups[3].replaceAll("\\s+", "").toLowerCase().split(",");
            Object[] columnValues = groups[6].replaceAll("'\\s+|\\s+'","'").split(",");
            if(columnNames.length != columnValues.length)
                throw new Exception();
            for (int i = 0; i < columnNames.length; i++) {
                ParametersValidator validator = new ParametersValidator();
                if(!validator.isColumnHere(Facade.currentDatabaseName, this.groups[1].toLowerCase(), columnNames[i])) {
                	System.out.println("Couldn't find: C -> " + columnNames[i] + " @ " + Facade.currentDatabaseName + " in " + this.groups[1].toLowerCase());
                    throw new Exception();
                }
                String value = (String) columnValues[i];
                if(value.contains("'")) {
                    value = value.replaceAll("'", "");
                    row.put(columnNames[i], value);
                }
                else{
                    value = value.replaceAll("\\s+","");
                    row.put(columnNames[i], Integer.parseInt(value));
                }
            }
            return row;
        } catch (Exception e){
            System.out.println("Invalid Update Query");
        }
        return null;
    }
}
