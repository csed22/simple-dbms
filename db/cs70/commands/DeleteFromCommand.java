package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.checker.*;

import java.util.ArrayList;
import java.util.HashMap;

public class DeleteFromCommand extends HasFileAccess implements ICommand {

	private String[] groups;

    public DeleteFromCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        int count = 0;
        ArrayList <HashMap <String, Object>> currentTable = loadCurrentTable(Facade.currentDatabaseName, this.groups[1]);
        if(this.groups[2] == null){
            count = currentTable.size();
            currentTable.clear();
        }
        else{
            IConditionChecker checker = new ConditionChecker(this.groups[3].toLowerCase());
            for(int i=0; i<currentTable.size(); i++){
                if(checker.checkCondition(currentTable.get(i))){
                    currentTable.remove(i--);
                    count++;
                }
            }
        }
        saveCurrentTable(Facade.currentDatabaseName, this.groups[1], currentTable);
        return count;
    }

}
