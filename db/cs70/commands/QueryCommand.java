package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.checker.ConditionChecker;
import eg.edu.alexu.csd.oop.db.cs70.tools.checker.IConditionChecker;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

import java.io.PrintStream;
import java.util.*;

public class QueryCommand extends HasFileAccess implements ICommand {

    private String [] groups;

    public QueryCommand(String [] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        ArrayList<HashMap <String, Object>> selectionResult = new ArrayList<>();
        String [] columnNames = this.groups[2].replaceAll("\\s","").toLowerCase().split(",");
        String tableName = this.groups[8].toLowerCase();
        ArrayList<HashMap<String, Object>> selectedTable = loadCurrentTable(Facade.currentDatabaseName, tableName);
        if(columnNames[0].equals("*")){
            columnNames = selectedTable.get(0).keySet().toArray(new String[0]);
        }
        ParametersValidator validator = new ParametersValidator();
        try {
            for (String columnName : columnNames) {
                if (!validator.isColumnHere(Facade.currentDatabaseName, tableName, columnName)) {
                    System.out.println("Column " + columnName + " does not exist in table " + tableName);
                    throw new Exception();
                }
            }
        }catch (Exception e){}
        IConditionChecker checker = new ConditionChecker(this.groups[10]);
        for(HashMap <String, Object> record: selectedTable) {
            if(checker.checkCondition(record)) {
                HashMap<String, Object> queryRecord = new HashMap<>();
                for (String column : columnNames) {
                    queryRecord.put(column, record.get(column));
                }
                selectionResult.add(queryRecord);
            }
        }
        Object[][] selectionResultArray = this.convertToArray(selectionResult);
        this.print1DArray(columnNames);
        this.print2DArray(selectionResultArray);
        return selectionResultArray;
    }

    private Object[][] convertToArray(ArrayList<HashMap <String, Object>> selectionResult){

        int numberOfRows = selectionResult.size();
        int numberOfColumns = 0;
        try {
            if(selectionResult.isEmpty())
                throw new NullPointerException();
            numberOfColumns = selectionResult.get(0).size();
        } catch (NullPointerException e){}

        Object[][] selectionResultArray = new Object[numberOfRows][numberOfColumns];

        for(int i=0; i < numberOfRows;i++) {
            int j = 0;
            for (Map.Entry<String, Object> stringObjectEntry : selectionResult.get(i).entrySet()) {
                selectionResultArray[i][j] = stringObjectEntry.getValue();
                j++;
            }
        }
        return selectionResultArray;
    }

    private void print2DArray(Object[][] selectionResultArray){
        for(Object [] selectionResultRow : selectionResultArray){
            for(Object selectionResultCell : selectionResultRow){
                PrintStream printStream = new PrintStream(System.out);
                printStream.printf("%20s\t",selectionResultCell.toString());
            }
            System.out.println();
        }
        System.out.println();
    }

    private void print1DArray(Object[] columnNames){
        System.out.println();
        for(Object columnName : columnNames){
            PrintStream printStream = new PrintStream(System.out);
            printStream.printf("%20s\t",columnName.toString().toUpperCase());
        }
        System.out.println();
        System.out.println();
    }
}
