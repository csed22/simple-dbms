package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.checker.*;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class UpdateSetCommand extends HasFileAccess implements ICommand {

	private String[] groups;

    public UpdateSetCommand(String[] groups) {
        this.groups = groups;
    }

	@Override
    public Object execute() throws SQLException{
        int count = 0;
        ParametersValidator validator = new ParametersValidator();
        if(!validator.isTableHere(Facade.currentDatabaseName, this.groups[1].toLowerCase())){
            System.out.println("Table hasn't been created yet!");
            throw new SQLException();
        }
        ArrayList<HashMap<String, Object>> currentTable = loadCurrentTable(Facade.currentDatabaseName, this.groups[1].toLowerCase());
        HashMap<String,Object> replacement = createReplacement(this.groups[2].replaceAll("\\s+"," ").split(","));
        if(this.groups[10] == null){
            for(HashMap<String, Object> currentRow : currentTable) {
                try {
                    if(!checkTableRows(currentRow))
                        throw new Exception();
                    this.replaceValues(replacement, currentRow);
                    count = currentTable.size();
                } catch (Exception e) {
                    System.out.println("Empty Table");
                }
            }
        }
        else{
            IConditionChecker checker = new ConditionChecker(this.groups[10]);
            for (HashMap<String, Object> currentRow : currentTable) {
                if (checker.checkCondition(currentRow)) {
                    try {
                        if(!checkTableRows(currentRow))
                            throw new Exception();
                        this.replaceValues(replacement, currentRow);
                        count++;
                    } catch (Exception e) {
                        System.out.println("Empty Table");
                    }
                }
            }
        }
        
        saveCurrentTable(Facade.currentDatabaseName, this.groups[1].toLowerCase(), currentTable);
        return count;
    }
    private HashMap<String, Object> createReplacement(String [] updates){
        HashMap<String,Object> replacement = new HashMap<>();
        for (String update : updates) {
            String[] assignmentParts = update.split("\\beq\\b");
            replacement.put(assignmentParts[0].replaceAll("\\s","").toLowerCase(),assignmentParts[1]);
        }
        return replacement;
    }

    private void replaceValues(HashMap<String, Object> replacement, HashMap<String, Object> target){
        String [] targetedColumnNames = replacement.keySet().toArray(new String[0]);
        ParametersValidator validator = new ParametersValidator();
        for(String targetedColumnName : targetedColumnNames){
            try {
                if(!validator.isColumnHere(Facade.currentDatabaseName, this.groups[1].toLowerCase(), targetedColumnName.toLowerCase()))
                    throw new Exception();
                Object replaceValue = replacement.get(targetedColumnName.toLowerCase());
                if(replaceValue.toString().contains("'")){
                    if(!validator.isDataType(Facade.currentDatabaseName, this.groups[1].toLowerCase(), targetedColumnName.toLowerCase(), String.class))
                        throw new Exception();
                    replaceValue = replaceValue.toString().replaceAll("\\s?'|'\\s?","");
                }
                else{
                    if(!validator.isDataType(Facade.currentDatabaseName, this.groups[1].toLowerCase(), targetedColumnName.toLowerCase(), int.class))
                        throw new Exception();
                    replaceValue = Integer.parseInt(replaceValue.toString().replaceAll("\\s",""));
                }
                target.replace(targetedColumnName.toLowerCase(), replaceValue);
            } catch (Exception e){}
        }
    }

    private boolean checkTableRows(HashMap<String, Object> row){
        for(int i=0; i<row.size(); i++) {
            if(row.get(row.keySet().toArray()[i]) == null)
                return false;
        }
        return true;
    }
}
