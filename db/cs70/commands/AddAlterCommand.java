package eg.edu.alexu.csd.oop.db.cs70.commands;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.tools.xml.*;

public class AddAlterCommand implements ICommand {

    private String[] groups;

    public AddAlterCommand(String[] groups) {
        this.groups = groups;
    }

    @Override
    public Object execute() {
        FileManager fileManager = new XMLFileManager();
        try {
            switch (this.groups[3].toLowerCase()) {
                case "int":
                    fileManager.addColumn(Facade.currentDatabaseName, this.groups[1].toLowerCase(), this.groups[2].toLowerCase(), Integer.class);
                    break;
                case "varchar":
                    fileManager.addColumn(Facade.currentDatabaseName, this.groups[1].toLowerCase(), this.groups[2].toLowerCase(), String.class);
                    break;
                default:
                    throw new Exception();

            }
        } catch (Exception e) {
            System.out.println("Invalid Data Type");
        }
        return 0;
    }
}
