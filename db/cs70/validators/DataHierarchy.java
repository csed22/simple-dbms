package eg.edu.alexu.csd.oop.db.cs70.validators;

import java.io.IOException;

import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.schema.HierarchySchemaTransformer;
import eg.edu.alexu.csd.oop.db.cs70.validators.schema.SchemaUtil;

public class DataHierarchy {

	private HierarchySchemaTransformer transformer;
	private SchemaUtil schemaManager;
	
	// static variable single_instance of type Singleton 
    private static DataHierarchy single_instance = null;
  
    // private constructor restricted to this class itself 
    private DataHierarchy() {
    	transformer  = new HierarchySchemaTransformer();
    	schemaManager = new SchemaUtil();
    	
    	try {
    		loadHierarchy();
		} catch (Exception e) {
			e.printStackTrace();
			initiateHierarchy();
		}
    }
  
    private void initiateHierarchy() {
		//System.out.println("loading hierarchy has failed, deleting all files to initiate a new hierarchy: " + cleanDataRecursively(new File(getParentDataFolderPath())));
    	Root.getInstance();
	}
    
    public void saveHierarchy() {
    	try {
			schemaManager.saveDataHierarchy(transformer.compressHierarchyToSchemaFile());
		} catch (IOException e) {
			System.out.println("Saving data hierarchy is failed!");
			e.printStackTrace();
		}
  	}
    
    private void loadHierarchy() {
    	try {
			transformer.extractHierarchyFromSchemaFile(schemaManager.loadDataHierarchy());
		} catch (IOException e) {
			System.out.println("Loading data hierarchy is failed!");
			e.printStackTrace();
		}
  	}

	// static method to create instance of Singleton class 
    public static DataHierarchy getInstance() { 
        if (single_instance == null) 
            single_instance = new DataHierarchy(); 
  
        return single_instance; 
    }
    
}

// https://www.geeksforgeeks.org/singleton-class-java/