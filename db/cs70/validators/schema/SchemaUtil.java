package eg.edu.alexu.csd.oop.db.cs70.validators.schema;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * uses beans.XMLEncoder / beans.XMLDecoder to save / load data.
 * @author Ahmed Waleed
 * @category Schema Utility
 */
public class SchemaUtil {
	
	 private final String schemaFile = "info";
	
	 public void saveDataHierarchy(SchemaFile info) throws IOException {

		String currentDir = getSchemaFilePath();
		
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(currentDir);
		} catch (Exception e) {
			System.out.println("The system cannot find the path specified: " + currentDir);
			return;
		}
		
		// https://howtodoinjava.com/java/serialization/xmlencoder-and-xmldecoder-example/
	    XMLEncoder encoder = new XMLEncoder(fos);
	    
	    encoder.setExceptionListener(new ExceptionListener() {
            public void exceptionThrown(Exception e) {
                System.out.println("Exception: " + e.toString());
            }
	    });
	    
	    // https://stackoverflow.com/questions/24725368/java-lang-instantiation-exception-while-using-xmlencoder
	    encoder.writeObject(info);
	    encoder.close();
	   
	    fos.close();

	 }

	 public SchemaFile loadDataHierarchy() throws IOException {

		String currentDir = getSchemaFilePath();
		SchemaFile info;
		
		InputStream fos = null;
		XMLDecoder decoder = null;
		
		try {
			
			fos = new FileInputStream(currentDir);
			
			// https://howtodoinjava.com/java/serialization/xmlencoder-and-xmldecoder-example/
			decoder = new XMLDecoder(fos);
			
			decoder.setExceptionListener(new ExceptionListener() {
	            public void exceptionThrown(Exception e) {
	                System.out.println("Exception:" + e.toString());
	            }
		    });
			
			try {
				/**
				 * XMLEncoder/Decoder go about this is by instantiating a default no-arg constructor
				 * and then reflectively calling the getters or setters for each one of the member objects.
				 * https://stackoverflow.com/questions/16531989/xml-serialization-and-empty-constructors
				 */
				info = (SchemaFile) decoder.readObject();
			} catch (Exception e) {
				System.out.println("Schema file is empty");
				info = new SchemaFile();
			}
			
			decoder.close();
			fos.close();
			
		} catch (Exception e) {
			//System.out.println("The system cannot find the path specified: " + currentDir);
			info = new SchemaFile();
		}

		return info;
	    
	 }

	 private String getSchemaFilePath() {
		return System.getProperty("user.dir") + "\\" + schemaFile + ".xml";
	 }
	
}

// https://howtodoinjava.com/java/serialization/xmlencoder-and-xmldecoder-example/