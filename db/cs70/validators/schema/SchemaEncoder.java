package eg.edu.alexu.csd.oop.db.cs70.validators.schema;

import java.util.ArrayList;

import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

public class SchemaEncoder {

	private SchemaFile info;
	
	public SchemaEncoder() {
		this.info = new SchemaFile();
	}
	
	public SchemaFile writeObject() {
		try {
			setTablesValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return info;
	}
	
	private void setTablesValues() {
		set1DTable ();
		set2DTable ();
		set3DTables();
	}
	
	private void set1DTable() {
		DBNode[] DBFromTree = Root.getInstance().getChildren();
		for(int i=0; i<DBFromTree.length; i++) {
				info.getDatabasesInfo().add(DBFromTree[i].getDbPath());
				//System.out.println("Encoded: DB-> " + info.getDatabasesInfo().get(i));
		}
	}
	private void set2DTable() {
		DBNode[] DBFromTree = Root.getInstance().getChildren();
		for(int i=0; i<DBFromTree.length; i++) {
			TNode[] TFromTree = DBFromTree[i].getChildren();
			info.getTablesInfo().add(new ArrayList<String>());
		for(int j=0; j<TFromTree.length; j++) {
				info.getTablesInfo().get(i).add(TFromTree[j].getTName());
				//System.out.println("Encoded: T-> " + info.getTablesInfo().get(i).get(j));
		}
		}
	}
	private void set3DTables() {
		DBNode[] DBFromTree = Root.getInstance().getChildren();
		for(int i=0; i<DBFromTree.length; i++) {
			TNode[] TFromTree = DBFromTree[i].getChildren();
			info.getColumnsInfo().add(new ArrayList<ArrayList<String>>());
		for(int j=0; j<TFromTree.length; j++) {
			CNode[] CFromTree = TFromTree[j].getChildren();
			info.getColumnsInfo().get(i).add(new ArrayList<String>());
		for(int k=0; k<CFromTree.length; k++) {
					info.getColumnsInfo().get(i).get(j).add(CFromTree[k].getCName() + "." + CFromTree[k].getCType());
					//System.out.println("Encoded: C-> " + info.getColumnsInfo().get(i).get(j).get(k));
		}
		}
		}
	}

}
