package eg.edu.alexu.csd.oop.db.cs70.validators.schema;

public class HierarchySchemaTransformer {

	public SchemaFile compressHierarchyToSchemaFile() {
		SchemaEncoder encoder = new SchemaEncoder();
		return encoder.writeObject();
	}
	
	public void extractHierarchyFromSchemaFile(SchemaFile info) {
		SchemaDecoder decoder = new SchemaDecoder(info);
		decoder.readObject();
	}
	
}
