package eg.edu.alexu.csd.oop.db.cs70.validators.schema;

import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

public class SchemaDecoder {

	private SchemaFile info;
	
	public SchemaDecoder(SchemaFile info) {
		this.info = info;
	}
	
	public void readObject() {
		
		setDBLevel();
		setTLevel();
		setCLevel();
		
	}
	
	private void setDBLevel() {
    	for(int i=0; i<info.getDatabasesInfo().size(); i++) {
    		Root.getInstance().addChild(new DBNode(info.getDatabasesInfo().get(i)));
    		//System.out.println("Decoded: DB-> " + info.getDatabasesInfo().get(i));
    	}
    }
    
    private void setTLevel() {
    	DBNode[] DBFromTree = Root.getInstance().getChildren();
    	for(int i=0; i<DBFromTree.length; i++) {
    		try {
    			for(int j=0; j<info.getTablesInfo().get(i).size(); j++) {
        			DBFromTree[i].addChild(new TNode(info.getTablesInfo().get(i).get(j)));
        			//System.out.println("Decoded: T-> " + info.getTablesInfo().get(i).get(j));
        		}
			} catch (Exception e) {
				System.out.println("There is no table at: " + DBFromTree[i].getDbPath());
			}
    		
    	}
	}

	private void setCLevel() {
		DBNode[] DBFromTree = Root.getInstance().getChildren();
    	for(int i=0; i<DBFromTree.length; i++) {
    		TNode[] TFromTree = DBFromTree[i].getChildren();
    		for(int j=0; j<TFromTree.length; j++) {
    			try {
    				for(int k=0; k<info.getColumnsInfo().get(i).get(j).size(); k++) {
            			TFromTree[j].addChild(new CNode(beforeTheDot(info.getColumnsInfo().get(i).get(j).get(k)),parseClass(info.getColumnsInfo().get(i).get(j).get(k))));
            			//System.out.println("Decoded: C-> " + info.getColumnsInfo().get(i).get(j).get(k));
            		}
				} catch (Exception e) {
					System.out.println("There is no column in: " + TFromTree[j].getTName() + " @ " +  DBFromTree[i].getDbPath());
				}
        	}
    	}
	}
	private String beforeTheDot(String columnDotType) {
		String s = "";
		
		for(int i=0; i<columnDotType.length(); i++) {
			if(columnDotType.charAt(i) == '.') {
				break;
			}
			s += columnDotType.charAt(i);
		}
		
		return s;
	}
	@SuppressWarnings("rawtypes")
	private Class parseClass(String columnDotType) {
		
		if(columnDotType.contains("String"))
			return String.class;
		
		return Integer.class;
	}
}
