package eg.edu.alexu.csd.oop.db.cs70.validators.schema;

import java.util.ArrayList;

public class SchemaFile {
	
	private ArrayList<ArrayList<ArrayList<String>>> columnsInfo;
	private ArrayList<ArrayList<String>>   			tablesInfo;
	private ArrayList<String>     					databasesInfo;
	
	public SchemaFile() {
		columnsInfo   = new ArrayList<ArrayList<ArrayList<String>>>();
		tablesInfo    = new ArrayList<ArrayList<String>>();
		databasesInfo = new ArrayList<String>();
	}
	
	public ArrayList<ArrayList<ArrayList<String>>> getColumnsInfo() {
		return columnsInfo;
	}

	public void setColumnsInfo(ArrayList<ArrayList<ArrayList<String>>> columnsInfo) {
		this.columnsInfo = columnsInfo;
	}

	public ArrayList<ArrayList<String>> getTablesInfo() {
		return tablesInfo;
	}

	public void setTablesInfo(ArrayList<ArrayList<String>> tablesInfo) {
		this.tablesInfo = tablesInfo;
	}

	public ArrayList<String> getDatabasesInfo() {
		return databasesInfo;
	}

	public void setDatabasesInfo(ArrayList<String> databasesInfo) {
		this.databasesInfo = databasesInfo;
	}
	
}
