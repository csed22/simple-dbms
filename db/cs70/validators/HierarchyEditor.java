package eg.edu.alexu.csd.oop.db.cs70.validators;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

public class HierarchyEditor {
	
	private HierarchyReader info = new HierarchyReader(); 
	
	public void registerDatabase(String path) {
		DBNode db = new DBNode(path);
		if(!info.isDatabaseHere(db.getDbName())) {
			Root.getInstance().addChild(db);
			//System.out.println(name + " DB is added");
			saveChanges();
		}else {
			System.out.println( db.getDbName() + " DB already exist!");
		}
		Facade.currentDatabaseName = db.getDbName();
	}
    public void registerTable(String db, String name) {
    	if(!info.isTableHere(db, name)) {
	    	info.fetchDatabase(db).addChild(new TNode(name));
	    	//System.out.println(name + " T is added");
	    	saveChanges();
    	}else
			System.out.println( name + " T already exist!");
    }
    @SuppressWarnings("rawtypes")
	public void registerColumn(String db, String t, String name, Class type) {
    	if(!info.isColumnHere(db, t, name)) {
	    	info.fetchTable(db, t).addChild(new CNode(name, type));
	    	//System.out.println(name + " C is added");
	    	saveChanges();
    	}else
    		System.out.println( name + " C already exist!");
    }
    
    
    public void renameDatabase(String oldName, String newName) {
		info.fetchDatabase(oldName).setDbName(newName);
		saveChanges();
	}
    public void renameTable(String db, String oldName, String newName) {
    	info.fetchTable(db, oldName).setTName(newName);
    	saveChanges();
    }
	public void renameColumn(String db, String t, String oldName, String newName) {
    	info.fetchColumn(db, t, oldName).setCName(newName);
    	saveChanges();
    }
    
	
    public void unregisterDatabase(String name) {
		Root.getInstance().removeChild(name);
		saveChanges();
	}
    public void unregisterTable(String db, String name) {
    	info.fetchDatabase(db).removeChild(name);
    	saveChanges();
    }
	public void unregisterColumn(String db, String t, String name) {
    	info.fetchTable(db, t).removeChild(name);
    	saveChanges();
    }
    
    private void saveChanges() {
    	DataHierarchy.getInstance().saveHierarchy();
    }
    
}
