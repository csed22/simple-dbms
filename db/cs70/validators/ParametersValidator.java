package eg.edu.alexu.csd.oop.db.cs70.validators;

import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

/**
 * has validation methods for all 3 levels of filing (db, t, c).
 * @author Ahmed Waleed
 * @category Validation Utility
 * @dPattern Facade
 */
public class ParametersValidator {
	
	private HierarchyReader info = new HierarchyReader();
	
	public ParametersValidator() {
		DataHierarchy.getInstance();
	}
	
	public void saveDataHierarchy() {
		DataHierarchy.getInstance().saveHierarchy();
	}
	
	public boolean isDatabaseHere(String db) {
		return info.isDatabaseHere(db);
	}
	public boolean isTableHere(String db, String t) {
		return info.isTableHere(db, t);
	}
	public boolean isColumnHere(String db, String t, String c) {
		return info.isColumnHere(db, t, c);
	}
	@SuppressWarnings("rawtypes")
	public boolean isDataType(String db, String t, String c, Class type) {
		return info.isDataType(db, t, c, type);
	}
	
	//Testing
	public void print() {
		
		System.out.println("Root: " + Root.getInstance());
		
		DBNode[] dbLevel = Root.getInstance().getChildren();
    	for(int i=0; i<dbLevel.length; i++) {
    		System.out.println("DB: " + dbLevel[i].getDbPath());
    		TNode[] TLevel = dbLevel[i].getChildren();
    		for(int j=0; j<TLevel.length; j++) {
    			System.out.println("\t* T: " + TLevel[j].getTName());
    			CNode[] CLevel = TLevel[j].getChildren();
        		for(int k=0; k<CLevel.length; k++) {
        			System.out.println("\t\t-> C: " + CLevel[k].getCName() + "->" + CLevel[k].getCType());
        		}
    		}
    	}
	}

}
