package eg.edu.alexu.csd.oop.db.cs70.validators.nodes;

import java.util.ArrayList;

/**
 * Database: Composite Node
 * @author Ahmed Waleed
 * @category Data Node
 */
public class DBNode implements DataNode {

	private String dbName;
	private String dbPath;
	private ArrayList<TNode> children;
	
	public DBNode() {
		setDbPath(new String());
		children = new ArrayList<TNode>();
	}
	
	public DBNode(String dbPath) {
		this.setDbPath(dbPath);
		dbName = extractDbName();
		children = new ArrayList<TNode>();
	}
	
	private String extractDbName() {
		try {
			String [] splitedPath = dbPath.split("\\\\|\\/");
			return splitedPath[splitedPath.length-1];
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void addChild(DataNode childNode) {
		if(children==null)
			children = new ArrayList<TNode>();
		children.add((TNode) childNode);
	}
	
	public void removeChild(String childName) {
		for(int i=0; i<children.size(); i++) 
			if(children.get(i).getTName().equalsIgnoreCase(childName))
				children.remove(i);
	}

	public TNode[] getChildren() {
		
		TNode[] childrenArr = new TNode[children.size()];
		
		for(int i=0; i< children.size(); i++) 
			childrenArr[i] = children.get(i);	
		
		return childrenArr;
		
	}
	
	public void setDbName(String dbName) {
		this.dbName = dbName;		
	}

	
	public void setChildren(ArrayList<TNode> children) {
		this.children = children;
	}

	public String getDbName() {
		return dbName;
	}

	public String getDbPath() {
		return dbPath;
	}

	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

}
