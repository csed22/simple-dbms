package eg.edu.alexu.csd.oop.db.cs70.validators.nodes;

/**
 * <<interface>>
 * @author Ahmed Waleed
 * @category Data Node
 * @dPattern Composite
 */
public interface DataNode {
	
	public void addChild(DataNode childNode);
	public void removeChild(String childName);
	
}
