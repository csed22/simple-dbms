package eg.edu.alexu.csd.oop.db.cs70.validators.nodes;

import java.util.ArrayList;

/**
 * Table: Composite Node
 * @author Ahmed Waleed
 * @category Data Node
 */
public class TNode implements DataNode {

	private String tName;
	private ArrayList<CNode> children;
	
	public TNode() {
		tName = new String();
		children = new ArrayList<CNode>();
	}
	
	public TNode(String name) {
		tName = name;
		children = new ArrayList<CNode>();
	}
	
	public void addChild(DataNode childNode) {
		if(children==null)
			children = new ArrayList<CNode>();
		children.add((CNode) childNode);
	}
	
	public void removeChild(String childName) {
		for(int i=0; i<children.size(); i++) 
			if(children.get(i).getCName().equalsIgnoreCase(childName))
				children.remove(i);
	}

	public CNode[] getChildren() {

		CNode[] childrenArr = new CNode[children.size()];
		
		for(int i=0; i< children.size(); i++) 
			childrenArr[i] = children.get(i);	
		
		return childrenArr;
		
	}

	public String getTName() {
		return tName.replace(".xml", "");
	}

	public void setTName(String tName) {
		this.tName = tName + ".xml";
	}

	public void setChildren(ArrayList<CNode> children) {
		this.children = children;
	}
	
}
