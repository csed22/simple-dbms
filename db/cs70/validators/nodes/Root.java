package eg.edu.alexu.csd.oop.db.cs70.validators.nodes;

import java.util.ArrayList;

/**
 * Root : Single Composite Node
 * @author Ahmed Waleed
 * @category Data Node
 * @dPattern Singleton
 */
public class Root implements DataNode {

	// static variable single_instance of type Singleton 
    private static Root single_instance = null;
    private ArrayList<DBNode> children;
  
    // private constructor restricted to this class itself 
    private Root() {
    	children = new ArrayList<DBNode>();
    }
  
    // static method to create instance of Singleton class 
    public static Root getInstance() { 
        if (single_instance == null) 
            single_instance = new Root(); 
  
        return single_instance; 
    }
    
    // static method to delete instance of Singleton class 
    public static void dropInstance() { 
       single_instance = null;
    }

	public void addChild(DataNode childNode) {
		children.add((DBNode) childNode);
	}

	public void removeChild(String childName) {
		for(int i=0; i<children.size(); i++) 
			if(children.get(i).getDbName().equalsIgnoreCase(childName))
				children.remove(i);	
	}
	
	public DBNode[] getChildren() {
		
		DBNode[] childrenArr = new DBNode[children.size()];
		
		for(int i=0; i< children.size(); i++) 
			childrenArr[i] = children.get(i);	
		
		return childrenArr;
		
	}

} 	

// https://www.geeksforgeeks.org/singleton-class-java/