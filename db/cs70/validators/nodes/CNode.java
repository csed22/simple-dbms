package eg.edu.alexu.csd.oop.db.cs70.validators.nodes;

/**
 * Column: Leaf Node
 * @author Ahmed Waleed
 * @category Data Node
 */
@SuppressWarnings("rawtypes")
public class CNode implements DataNode {
	
	private String cName;
	private Class cType;
	
	public CNode() {
		cName = new String();
		cType = null;
	}
	
	public CNode(String name, Class type) {
		cName = name;
		cType = type;
	}

	public void addChild(DataNode childNode) {
		// Sorry interface segregation principle, but i do nothing.
	}
	public void removeChild(String childName) {
		// Sorry interface segregation principle, but i do nothing.
	}

	public String getCName() {
		return cName;
	}
	
	public void setCName(String cName) {
		this.cName = cName;
	}
	
	public Class getCType() {
		return cType;
	}
	
	public void setCType(Class type) {
		cType = type;
	}
	
}
