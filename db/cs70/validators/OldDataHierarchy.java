package eg.edu.alexu.csd.oop.db.cs70.validators;

import eg.edu.alexu.csd.oop.db.cs70.tools.xml.DirUtil;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

/**
 * connects composites of DataNodes (/, db, t) and single DataNodes (c),
 * it is updated at run time, for sorry this won't work.
 * @author Ahmed Waleed
 * @category Validation Utility
 * @dPattern Singleton, Composite
 */
public class OldDataHierarchy {
	
	private DirUtil dirManager;
	
	// static variable single_instance of type Singleton 
    private static OldDataHierarchy single_instance = null;
  
    // private constructor restricted to this class itself 
    private OldDataHierarchy() {
    	dirManager = new DirUtil();
    	updateHierarchy();
    }
  
    // static method to create instance of Singleton class 
    public static OldDataHierarchy getInstance() { 
        if (single_instance == null) 
            single_instance = new OldDataHierarchy(); 
  
        return single_instance; 
    }
    
    public void updateHierarchy() {
    	resetRoot();
    	linkNodes();
    }
    
    private void resetRoot() {
    	Root.dropInstance();
    	Root.getInstance();
    }
    
    private void linkNodes() {
    	setDBLevel();
    	setTLevel();
    	setCLevel();
    }

	private void setDBLevel() {
    	String[] dbArr = dirManager.getDatabases();
    	for(int i=0; i<dbArr.length; i++) {
    		Root.getInstance().addChild(new DBNode(dbArr[i]));
    		//System.out.println(dbArr[i]);
    	}
    }
    
    private void setTLevel() {
    	DBNode[] dbLevel = Root.getInstance().getChildren();
    	for(int i=0; i<dbLevel.length; i++) {
    		String[] TArr = dirManager.getTablesOfDB(dbLevel[i].getDbName());
    		for(int j=0; j<TArr.length; j++) {
    			dbLevel[i].addChild(new TNode(TArr[j]));
    			//System.out.println(TArr[j]);
    		}
    	}
	}

	private void setCLevel() {
		DBNode[] dbLevel = Root.getInstance().getChildren();
    	for(int i=0; i<dbLevel.length; i++) {
    		TNode[] TLevel = dbLevel[i].getChildren();
    		for(int j=0; j<TLevel.length; j++) {
        		String[] CArr = dirManager.getColumnsOfT(dbLevel[i].getDbName(), TLevel[j].getTName());
        		for(int k=0; k<CArr.length; k++) {
        			TLevel[j].addChild(new CNode(CArr[k], Object.class));
        			//System.out.println(CArr[k]);
        		}
        	}
    	}
	}

}

// https://www.geeksforgeeks.org/singleton-class-java/