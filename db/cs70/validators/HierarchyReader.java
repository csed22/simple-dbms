package eg.edu.alexu.csd.oop.db.cs70.validators;

import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.DBNode;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.Root;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.TNode;

public class HierarchyReader {
	
	public DBNode fetchDatabase(String db) {
		try {
			DBNode[] dbLevel = Root.getInstance().getChildren();
			for(int i=0; i<dbLevel.length; i++)
	    		if(dbLevel[i].getDbName().equalsIgnoreCase(db))
	    			return dbLevel[i];
		} catch (Exception e) {
			System.out.println("Couldn't reach " + db);
			//e.printStackTrace();
		}
		return null;
	}
	public TNode fetchTable(String db, String t) {
		try {
			DBNode[] dbLevel = Root.getInstance().getChildren();
			for(int i=0; i<dbLevel.length; i++) {
				if(dbLevel[i].getDbName().equalsIgnoreCase(db)) {
					TNode[] TLevel = dbLevel[i].getChildren();
		    		for(int j=0; j<TLevel.length; j++) {
			    		if(TLevel[j].getTName().equalsIgnoreCase(t))
			    			return TLevel[j];
		    		}
				}
			}
		} catch (Exception e) {
			System.out.println("Couldn't reach " + db + "/" + t);
			e.printStackTrace();
		}
		return null;
	}
	public CNode fetchColumn(String db, String t, String c) {
		try {
			DBNode[] dbLevel = Root.getInstance().getChildren();
			for(int i=0; i<dbLevel.length; i++) {
				if(dbLevel[i].getDbName().equalsIgnoreCase(db)) {
					TNode[] TLevel = dbLevel[i].getChildren();
		    		for(int j=0; j<TLevel.length; j++) {
			    		if(TLevel[j].getTName().equalsIgnoreCase(t)) {
			    			CNode[] CLevel = TLevel[j].getChildren();
				    		for(int k=0; k<CLevel.length; k++) {
					    		if(CLevel[k].getCName().equalsIgnoreCase(c))
					    			return CLevel[k];
				    		}
			    		}
		    		}
				}
			}
		} catch (Exception e) {
			System.out.println("Couldn't reach " + db + "/" + t + "/" + c);
			e.printStackTrace();
		}
		return null;
	}
	@SuppressWarnings("rawtypes")
	public Class fetchColumnDataType(String db, String t, String c) {
		CNode column = fetchColumn(db, t, c);
		
		if(column!=null)
			return column.getClass();
		
		return null;
	}

	public boolean isDatabaseHere(String db) {
		if(fetchDatabase(db)!=null)
			return true;
		return false;
	}
	public boolean isTableHere(String db, String t) {
		if(fetchTable(db, t)!=null)
			return true;
		return false;
	}
	public boolean isColumnHere(String db, String t, String c) {
		if(fetchColumn(db, t, c)!=null)
			return true;
		return false;
	}
	@SuppressWarnings("rawtypes")
	public boolean isDataType(String db, String t, String c, Class type) {
		if(fetchColumnDataType(db, t, c)!=null)
			return true;
		return false;
	}
	
}
