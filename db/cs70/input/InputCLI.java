package eg.edu.alexu.csd.oop.db.cs70.input;

import java.io.IOException;

import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;

/**
 * iterates over the input char by char.
 * @author Ahmed
 * @category Scanning Input
 */
public class InputCLI {
	
	// https://www.barcodefaq.com/ascii-chart-char-set/
	// when enter is pressed, it produces two characters: carriageReturn '\r' &  lineFeed '\n'
	private final char lineFeed = 10;	
	private final char carriageReturn = 13;	
	
	public String readScreenInput() {
		String queryText = "";
		try {
			queryText = inputLoop();
		} catch (IOException e) {}		
		
		return queryText;
	}
	
	private String inputLoop() throws IOException {
		String beforeSemicolon = "";
		
		beforeSemicolon = checkIfEnterPressed(beforeSemicolon);
		
		if(beforeSemicolon.contains(";"))
			return beforeSemicolon;
		
		if(!isAnOtherCommandInLine())
			startNewLine();
		
		beforeSemicolon = readAllBeforeSemicolon(beforeSemicolon);
		//beforeSemicolon = addSemicolon(beforeSemicolon);

		return beforeSemicolon;	
	}
	
	private boolean isAnOtherCommandInLine() {
		return !isEnterPressed;
	}
	
	//if true then an enter is pressed between two semicolons
	private boolean isEnterPressed = true;
	
	/**
	 * since the enter key is considered as two characters '\r' and '\n'
	 * the first two characters in any query need to be handled separately
	 * @return first 1-2 characters in the next query in the same line
	 */
	private String checkIfEnterPressed(String s) throws IOException {
		char i;
		isEnterPressed = true;
		
		i = (char) System.in.read();
		// check if it is not a carriageReturn, but generalized to skip first space as well
		// https://www.geeksforgeeks.org/character-iswhitespace-method-in-java-with-examples/
		if(!Character.isWhitespace(i)) {
			s += i;
			if(i == ';')
				return s;
			isEnterPressed = false;
		}
		
		i = (char) System.in.read();
		if(i != lineFeed) {
			if(i == carriageReturn) {
				startNewLine();
				i = (char) System.in.read();
				s += ' ';
				isEnterPressed = true;
			}else
				s += i;
			
			isEnterPressed = false;
		}
		
		return s;
	}
	
	private String readAllBeforeSemicolon(String s) throws IOException {
		char i;
		
		while((i = (char) System.in.read()) != ';') {
			if(i == carriageReturn)
				startNewLine();
			else if (i == lineFeed)
				s += ' ';
			else
				s += i;
		}
		
		return s;
	}
	
	@SuppressWarnings("unused")
	private String addSemicolon(String s) {
		return s + ';';
	}

	private void startNewLine() {
		if(Facade.currentDatabaseName!=null)
			System.out.print(Facade.currentDatabaseName + "> ");
		else
			System.out.print("> ");
	}
	
}
