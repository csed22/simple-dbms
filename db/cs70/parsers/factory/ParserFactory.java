package eg.edu.alexu.csd.oop.db.cs70.parsers.factory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eg.edu.alexu.csd.oop.db.cs70.parsers.IParser;
import eg.edu.alexu.csd.oop.db.cs70.parsers.Parser;
import eg.edu.alexu.csd.oop.db.cs70.parsers.strategies.CommandParserStrategy;
import eg.edu.alexu.csd.oop.db.cs70.parsers.strategies.QueryParserStrategy;

public class ParserFactory implements IParserFactory {

    @Override
    public IParser getParser(String input) {
        String regex = "\\s*(?i)select(?-i)[^@]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if(matcher.find())
            return new Parser(new QueryParserStrategy(), input);
        return new Parser(new CommandParserStrategy(), input);
    }
    
}
