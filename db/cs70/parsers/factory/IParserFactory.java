package eg.edu.alexu.csd.oop.db.cs70.parsers.factory;

import eg.edu.alexu.csd.oop.db.cs70.parsers.IParser;

public interface IParserFactory {
    IParser getParser(String input);
}
