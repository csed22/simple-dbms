package eg.edu.alexu.csd.oop.db.cs70.parsers;

import java.sql.SQLException;

import eg.edu.alexu.csd.oop.db.cs70.parsers.strategies.ParserStrategy;

public class Parser implements IParser {

    private ParserStrategy strategy;
    private String input;

    public Parser(ParserStrategy strategy, String input) {
        this.strategy = strategy;
        this.input = input;
    }

    @Override
    public void parse() {
        strategy.parse(input);
    }

    @Override
    public boolean isSuccessful() {
        return strategy.isSuccessful();
    }

    @Override
    public String[] getGroups() {
        return strategy.getGroups();
    }

    @Override
    public Object executeCommand() throws SQLException {
        return this.strategy.executeCommand();
    }
}
