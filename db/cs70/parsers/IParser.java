package eg.edu.alexu.csd.oop.db.cs70.parsers;

import java.sql.SQLException;

public interface IParser {
    void parse();

    boolean isSuccessful();

    String [] getGroups();

    Object executeCommand() throws SQLException;
}
