package eg.edu.alexu.csd.oop.db.cs70.parsers.strategies;

import eg.edu.alexu.csd.oop.db.cs70.commands.QueryCommand;

public class QueryParserStrategy extends ParserStrategy {

    @Override
    public void parse(String input) {
        //group 1: distinct
        //group 2: column names
        //group 8: table
        //group 10: where condition
        //group 18: joined table
        //group 20: on condition
        //group 28: order by columns
        //group 34: offset
        //group 38: limit
        String pattern = "^\\s*(?i)select(?-i)((?i)\\s+distinct(?-i))?\\s+(\\*|((\\w+(\\.\\w+)?\\s*\\,\\s*)*(\\w+" +
                "(\\.\\w+)?)))\\s+(?i)from(?-i)\\s+(\\w+)(\\s+(?i)where(?-i)\\s+(((\\B(\\'[^']+\\')\\B|\\b\\d+\\b|" +
                "\\b\\w+\\b)\\s*){3}(\\s+((?i)(and)(?-i)|(?i)(or)(?-i))\\s+((\\B(\\'[^']+\\')\\B|\\b\\d+\\b|\\b\\w" +
                "+\\b)\\s*){3})*))?(\\s*(?i)join(?-i)\\s+(\\w+(\\.\\w+)?)\\s+(?i)on(?-i)\\s+(((\\B(\\'[^']+\\')\\B|" +
                "\\b\\d+\\b|\\b\\w+\\b)\\s*){3}(\\s+((?i)(and)(?-i)|(?i)(or)(?-i))\\s+((\\B(\\'[^']+\\')\\B|\\b\\d+\\" +
                "b|\\b\\w+\\b)\\s*){3})*))?(\\s+(?i)order\\s+by(?-i)\\s+((\\w+(\\.\\w+)?\\s*\\,\\s*)*(\\w+(\\.\\w+)?)" +
                "))?(\\s+(?i)offset(?-i)\\s+(\\d+))?(\\s+(?i)limit(?-i)\\s+(\\d+))?\\s*$";
       this.regexAction(pattern, input);
       if(this.isSuccessful()){
           this.setCommand(new QueryCommand(this.getGroups()));
       }
       else{
           try {
               throw new Exception();
           } catch (Exception e) {
               System.out.println("Invalid Query");
           }
       }
    }
}
