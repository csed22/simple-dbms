package eg.edu.alexu.csd.oop.db.cs70.parsers.strategies;

import eg.edu.alexu.csd.oop.db.cs70.DatabaseImpl;
import eg.edu.alexu.csd.oop.db.cs70.commands.database.DBMSCreateDatabaseCommand;
import eg.edu.alexu.csd.oop.db.cs70.commands.database.DBMSQueryCommand;
import eg.edu.alexu.csd.oop.db.cs70.commands.database.DBMSStructureQueryCommand;
import eg.edu.alexu.csd.oop.db.cs70.commands.database.DBMSUpdateQueryCommand;

public class InitialParserStrategy extends ParserStrategy {
    @Override
	public void parse(String input) {
        String queryRegex = "^\\s*(?i)select(?-i)[^@]+";
        String createDatabaseRegex = "^\\s*(?i)create(?-i)\\s+(?i)database(?-i)\\s+([^;]+)\\s*$";
        String structureQueryRegex = "^\\s*((?i)create(?-i)|(?i)drop(?-i))[^@]+";
        String updateQueryRegex = "";
        String [] patterns = new String[]{queryRegex, createDatabaseRegex, structureQueryRegex, updateQueryRegex};
        for(int i=0; i<patterns.length; i++){
            this.regexAction(patterns[i], input);
            if(this.isSuccessful()) {
                switch (i){
                    case 0:
                        this.setCommand(new DBMSQueryCommand(new DatabaseImpl(), input));
                        break;
                    case 1:
                        this.setCommand(new DBMSCreateDatabaseCommand(new DatabaseImpl(), this.getGroups()[1]));
                        break;
                    case 2:
                        this.setCommand(new DBMSStructureQueryCommand(new DatabaseImpl(), input));
                        break;
                    case 3:
                        this.setCommand(new DBMSUpdateQueryCommand(new DatabaseImpl(), input));
                        break;
                }
                break;
            }
        }
    }
}
