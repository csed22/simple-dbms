package eg.edu.alexu.csd.oop.db.cs70.parsers.strategies;

import eg.edu.alexu.csd.oop.db.cs70.commands.*;

public class CommandParserStrategy extends ParserStrategy {

    @Override
    public void parse(String input) {
        String createDatabaseRegex =  "^\\s*(?i)create(?-i)\\s+(?i)database(?-i)\\s+([^;]+)\\s*$";
        String dropDatabaseRegex = "^\\s*(?i)drop(?-i)\\s+(?i)database(?-i)\\s+([^;]+)\\s*$";
        String createTableRegex = "^\\s*(?i)create(?-i)\\s+(?i)table(?-i)\\s+(\\w+)\\s*\\(\\s*((\\" +
                "w+\\s+\\w+\\s*(\\s+(?i)not(?-i)\\s+(?i)null(?-i))?\\s*(\\s+(?i)primary(?-i)\\s+(?i)" +
                "key(?-i))?\\s*\\,\\s*)*(\\w+\\s+\\w+\\s*(\\s+(?i)not(?-i)\\s+(?i)null(?-i))?\\s*(\\" +
                "s+(?i)primary(?-i)\\s+(?i)key(?-i))?\\s*)?)\\)\\s*$";
        String dropTableRegex = "^\\s*(?i)drop(?-i)\\s+(?i)table(?-i)\\s+(\\w+)\\s*$";
        String insertIntoRegex = "^\\s*(?i)insert(?-i)\\s+(?i)into(?-i)\\s+(\\w+)\\s*(\\(\\s*((\\w+\\s*\\,\\s*)" +
                "*(\\w+\\s*))\\))?\\s+(?i)values(?-i)\\s*\\(\\s*((((\\'[^']+\\')|\\d+)\\s*\\,\\s*)*?(((\\'[^']+\\')" +
                "|\\d+\\s*)))\\)\\s*$";
        String deleteFromRegex = "^\\s*(?i)delete(?-i)\\s+(?i)from(?-i)\\s+(\\w+)\\s*(\\s+(?i)where(?-i)\\s+((" +
                "(\\B(\\'[^']+\\')\\B|\\b\\d+\\b|\\b\\w+\\b)\\s*){3}(\\s+((?i)(and)(?-i)|(?i)(or)(?-i))\\s+((\\" +
                "B(\\'[^']+\\')\\B|\\b\\d+\\b|\\b\\w+\\b)\\s*){3})*))?\\s*$";
        String updateSetRegex = "^\\s*(?i)update(?-i)\\s+(\\w+)\\s+(?i)set(?-i)\\s+((\\w+\\s*eq\\s*((\\'[^']+\\')|\\d+)\\s*\\,\\s*)*" +
        		"(\\w+\\s*eq\\s*((\\'[^']+\\')|\\d+)\\s*))(\\s+(?i)where(?-i)\\s+((((\\B(\\'[^']+\\')\\B|\\b\\d+\\b|\\b\\w+\\b))\\s*){3}" +
        		"(\\s+((?i)(and)(?-i)|(?i)(or)(?-i))\\s+((\\B(\\'[^']+\\')\\B|\\b\\d+\\b|\\b\\w+\\b)\\s*){3})*))?\\s*$";
        String alterAddRegex = "^\\s*(?i)alter(?-i)\\s+(?i)table(?-i)\\s+(\\w+)\\s+(?i)add(?-i)\\s+(\\w+)\\s+(\\w+)\\s*$";
        String alterDropRegex = "^\\s*(?i)alter(?-i)\\s+(?i)table(?-i)\\s+(\\w+)\\s+(?i)drop(?-i)\\s+(?i)column(?-i)\\s+(\\w+)\\s*$";
        String alterModifyRegex = "^\\s*(?i)alter(?-i)\\s+(?i)table(?-i)\\s+(\\w+)\\s+(?i)modify(?-i)\\s+(\\w+)\\s+(\\w+)\\s*$";
        String exceptionThrower = "";
        String [] patterns = new String[]{createDatabaseRegex,dropDatabaseRegex,createTableRegex,dropTableRegex,insertIntoRegex,
                deleteFromRegex,updateSetRegex, alterAddRegex, alterDropRegex, alterModifyRegex,exceptionThrower};

        for(int i=0; i<patterns.length; i++){
            this.regexAction(patterns[i], input);
            if(this.isSuccessful()) {
                switch (i+1){
                    case 1:
                        setCommand(new CreateDatabaseCommand(this.getGroups()));
                        break;
                    case 2:
                        setCommand(new DropDatabaseCommand(this.getGroups()));
                        break;
                    case 3:
                        setCommand(new CreateTableCommand(this.getGroups()));
                        break;
                    case 4:
                        setCommand(new DropTableCommand(this.getGroups()));
                        break;
                    case 5:
                        setCommand(new InsertIntoCommand(this.getGroups()));
                        break;
                    case 6:
                        setCommand(new DeleteFromCommand(this.getGroups()));
                        break;
                    case 7:
                        setCommand(new UpdateSetCommand(this.getGroups()));
                        break;
                    case 8:
                        setCommand(new AddAlterCommand(this.getGroups()));
                        break;
                    case 9:
                        setCommand(new DropAlterCommand(this.getGroups()));
                        break;
                    default:
                        try {
                            throw new Exception();
                        } catch (Exception e) {
                            this.setSuccess(false);
                            System.out.println("Invalid Command");
                        }
                }
                break;
            }
        }
    }
}
