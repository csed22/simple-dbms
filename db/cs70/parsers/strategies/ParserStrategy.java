package eg.edu.alexu.csd.oop.db.cs70.parsers.strategies;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eg.edu.alexu.csd.oop.db.cs70.commands.ICommand;

abstract public class ParserStrategy {

    protected boolean success;
    protected String[] groups;
    protected ICommand command;

    public abstract void parse(String input);

    public boolean isSuccessful() {
        return this.success;
    }

    public String[] getGroups() {
        return this.groups;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setGroups(String[] groups) {
        this.groups = groups;
    }

    public void setCommand(ICommand command) { this.command = command; }

    public Object executeCommand() throws SQLException { return this.command.execute(); }

    protected boolean regexAction(String pattern, String input){
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(input);
        this.setSuccess(matcher.find());
        if(this.isSuccessful()) {
            String[] temp = new String[matcher.groupCount()+1];
            for (int j = 0; j < temp.length; j++)
                temp[j] = matcher.group(j);
            this.setGroups(temp);
        }
        return this.isSuccessful();
    }
}
