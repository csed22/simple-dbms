package eg.edu.alexu.csd.oop.db.cs70.tools.time;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eg.edu.alexu.csd.oop.db.cs70.controllers.Controller;
import eg.edu.alexu.csd.oop.db.cs70.controllers.TimeOutController;

/**
 * offers to kill the program if a command takes too long
 * @author Ahmed Waleed
 * @category Time Utility
 */
@SuppressWarnings("serial")
public class TimeOutCInterrupter extends JPanel implements ActionListener {

	public Controller userPreferences;
	
	private JLabel background;
	private ImageIcon image;
	
	private JFrame window;
	private JButton continueButton;
	private JButton killButton;

	public TimeOutCInterrupter() {
		
		userPreferences = new TimeOutController();
		setBounds(0,0,295,171);
        setVisible(true);
        setLayout(null);
        
        // http://www.java2s.com/Code/JavaAPI/javax.swing/JLabelsetIconIconicon.htm
        image = new ImageIcon(getClass().getResource("interrupter.jpg"));
        background = new JLabel(image);
        background.setBounds(0,0,295,171);
        add(background);
        
        // it does not appear unless the mouse is hovered above it,
        // it was a problem in paint ( used timer to refresh the pane ),
        // the solution didn't work, but it is nice to keep it that way.
        continueButton = new JButton("Continue at your own risk!");
    	continueButton.setBounds(10,130,181,30);
    	//continueButton.setBorder(new RoundedBorder(5));
    	continueButton.addActionListener(this);
        add(continueButton);
        
        // https://www.javaworld.com/article/2077368/a-multiline-button-is-possible.html
        // https://stackoverflow.com/questions/15746970/how-to-add-a-multi-line-text-to-a-jbutton-with-the-line-unknown-dynamically
        // https://www.tutorialspoint.com/How-to-set-Text-alignment-in-HTML
        
        // mixing the information and code from the 3 links above
        killButton = new JButton("<html>" + "<p style=\"text-align:center;\">Kill</p>" + "the program" + "</html>");
        killButton.setBounds(10,50,110,60);
        killButton.addActionListener(this);
        add(killButton);
        
        //repaint();
	
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {

		Object source = event.getSource();
		
		 if (source == continueButton) {
			 userPreferences.deny();
			 disposeWindow();
		 }
		 if (source == killButton) {
			 userPreferences.allow();
			 System.exit(0);
		 }
		
	}
	
	public void disposeWindow() {
		if(window!= null && window.isVisible()) {
			window.setVisible(false); // you can't see me!
			window.dispose(); // Destroy the JFrame object
		}
		// https://stackoverflow.com/questions/1234912/how-to-programmatically-close-a-jframe		
	}
	
	public void run() {
    	
		window = new JFrame();
        
        window.setSize(295,171);
		window.setLocationRelativeTo(null);
        
        window.setUndecorated(true);
	    // https://stackoverflow.com/questions/8701716/how-to-remove-title-bar-in-jframe
		window.setShape(new RoundRectangle2D.Double(0, 0, 295, 171, 5, 5));
		// https://stackoverflow.com/questions/18935558/java-rounded-corners-on-jframe
        
		window.add(this);	// must add it after setting the shape.
		
		 try {
			 	window.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("warning.png")));
	            // https://www.flaticon.com/free-icon/lights_247826
			} catch (Exception e) {
				System.out.println("Coudldn't find icon: " + ClassLoader.getSystemResource("warning.png"));
			}
        
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	}
	
}
