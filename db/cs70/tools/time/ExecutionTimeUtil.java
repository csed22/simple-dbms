package eg.edu.alexu.csd.oop.db.cs70.tools.time;

import java.util.Timer;
import java.util.TimerTask;

/**
 * tracks execution time, and controls the Interrupter
 * @author Ahmed Waleed
 * @category Time Utility
 */
public class ExecutionTimeUtil {

	private long startTime;
	private long endTime;
	
	private Timer updateTimer;
	private TimeOutCInterrupter trackTime;
	
	private static final int TIME_LIMIT = 5000; 
	
	public ExecutionTimeUtil() {
		startTime = System.currentTimeMillis();
		trackTime = new TimeOutCInterrupter();
		trackTime.userPreferences.allow();
		startTickTock();
	}
	
	/**
	 * @return Elapsed Time in milliseconds
	 */
	public long elapsedTime() {
		endTime = System.currentTimeMillis();
		trackTime.disposeWindow();
		updateTimer.cancel();
		return endTime-startTime;
	}
	
	private void startTickTock() {
		TimerTask advanceTime = new TimerTask() {
			
			public void run() {
				
				endTime = System.currentTimeMillis();
				checkIftimeOut();
				
			}
		};
		updateTimer = new Timer();
		updateTimer.scheduleAtFixedRate(advanceTime, 0, 100);
	}
	
	private void checkIftimeOut() {
		if( endTime-startTime > TIME_LIMIT ) {
			if(trackTime.userPreferences.isAllowedToOpen()) {
				trackTime.userPreferences.deny();
				trackTime.run();
			}
		}
	}
	
}

// https://www.techiedelight.com/measure-elapsed-time-execution-time-java/