package eg.edu.alexu.csd.oop.db.cs70.tools.files;

import java.util.Formatter;
/**
 * writes any file, given its String and location
 * @author Ahmed Waleed
 * @category FileHandlingClass
 */
public class FileWriter {
	
	private String fileLocation;
	private Formatter   create;
	
	public FileWriter(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
	private void openFile() {
		
		try {
			create = new Formatter(fileLocation);
		} catch (Exception e) {}
		
	}
	
	private void addLine(String str) {
		create.format("%s\n",str);
	}
	
	private void closeFile() {
		create.close();
	}
	
	/**
	 * @param fileText String to be written 
	 */
	public void write(String[] fileText) {
		openFile();
		for(int i=0; i<fileText.length; i++)
			addLine(fileText[i]);
		closeFile();
	}
	
} //https://www.youtube.com/watch?v=Bws9aQuAcdg&list=PLFE2CE09D83EE3E28&index=81&t=0s