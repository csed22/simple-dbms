package eg.edu.alexu.csd.oop.db.cs70.tools.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
/**
 * reads a file that contains a table as 2D String array
 * @author Ahmed Waleed
 * @category FileHandlingClass
 */
public class FileReader {
	
	protected String fileLocation;
	protected final String DELIMITER;
	protected int dataColumnsCount;
	
	public FileReader(String fileLocation, String delimiter, int dataColumnsCount) {
		this.fileLocation = fileLocation;
		this.DELIMITER = delimiter;
		this.dataColumnsCount = dataColumnsCount;
	}
	
	private Scanner initiateFile(Scanner input, String location) throws FileNotFoundException {
		
		input = new Scanner(new File(location));
		return input;
		
	} //https://www.youtube.com/watch?v=3RNYUKxAgmw&list=PLFE2CE09D83EE3E28&index=82&t=0s
	
	private int lineCounter (String location) throws IOException{
		int lineCount = 0;
		
        Path path = Paths.get(location);
        lineCount = (int) Files.lines(path).count();
        
		return lineCount;
		
    } //https://stackoverflow.com/questions/1277880/how-can-i-get-the-count-of-line-in-a-file-in-an-efficient-way/1277904
	
	private String[] readLine(Scanner input) {
		
		String data = input.nextLine();
		String[] values = data.split(DELIMITER);
		
		return values;
		
	} //https://www.w3schools.com/jsref/jsref_split.asp
	
	private String[][] readFile(Scanner input, int rows, int columns) {
		
		String[][] tmpArray = new String[rows][columns];
		String  [] tmpLine	= new String      [columns];
		
		int n = 0;
		
		while(input.hasNext()) {
			tmpLine = readLine(input);
			tmpArray[n] = tmpLine;
			n++;
		}
		return tmpArray;
		
	} //https://youtu.be/3_40oiUdLG8
	
	/**
	 * reads any file that contains a table with its data
	 * separated by a known delimiter.  
	 * @return 2D String array representing the table
	 */
	public String[][] read() throws IOException {
		
		Scanner inputScanner = null;
		inputScanner = initiateFile(inputScanner,fileLocation);
		
		int dataRowsCount = lineCounter(fileLocation);
		
		return readFile(inputScanner,dataRowsCount,dataColumnsCount);
		
	}
	
}