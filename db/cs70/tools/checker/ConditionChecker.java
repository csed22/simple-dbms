package eg.edu.alexu.csd.oop.db.cs70.tools.checker;

import java.util.HashMap;

public class ConditionChecker implements IConditionChecker {

    private String condition = null;

    public ConditionChecker(String condition) {
        if(condition != null)
            this.condition = condition.replaceAll("\\s+"," ");
    }

    @Override
    public boolean checkCondition(HashMap<String, Object> row) {
        boolean returnValue = true;
        if(condition == null)
            return true;
        String[] disjunctions = condition.split("\\b(?i)or(?-i)\\b");
        for (String disjunction : disjunctions) {
            String[] conjunctions = disjunction.split("\\b(?i)and(?-i)\\b");
            for (String conjunction : conjunctions) {
                returnValue = true;
                returnValue = returnValue && checkSingleCondition(row, conjunction);
                if(!returnValue)
                    break;
            }
            if (returnValue)
                break;
        }
        return returnValue;
    }

    public boolean checkSingleCondition(HashMap<String, Object> row, String condition) {
        condition = condition.replaceAll("^\\s","");
        String[] conditionParts = condition.split("\\s");
        String columnName = conditionParts[0].toLowerCase();
        String relation = conditionParts[1];
        if(conditionParts[2].contains("'")) {
            String value = conditionParts[2].replaceAll("'", "");
            switch (relation) {
                case "eq":
                    return (row.get(columnName).toString()).compareTo(value) == 0;
                case "neq":
                    return (row.get(columnName).toString()).compareTo(value) != 0;
                case "greq":
                    return (row.get(columnName).toString()).compareTo(value) >= 0;
                case "leq":
                    return (row.get(columnName).toString()).compareTo(value) <= 0;
                case "less":
                    return (row.get(columnName).toString()).compareTo(value) < 0;
                case "gre":
                    return (row.get(columnName).toString()).compareTo(value) > 0;
                default:
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                        System.out.println("Invalid Comparison Operator in WHERE Statement");
                        ;
                    }
            }
        } else {
            int value = Integer.parseInt(conditionParts[2]);
            switch (relation) {
                case "eq":
                    return (int) row.get(columnName) == value;
                case "neq":
                    return (int) row.get(columnName) != value;
                case "greq":
                    return (int) row.get(columnName) >= value;
                case "leq":
                    return (int) row.get(columnName) <= value;
                case "less":
                    return (int) row.get(columnName) < value;
                case "gre":
                    return (int) row.get(columnName) > value;
                default:
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                        System.out.println("Invalid Comparison Operator in WHERE Statement");
                        ;
                    }
            }
        }
        return false;
    }
}
