package eg.edu.alexu.csd.oop.db.cs70.tools.checker;

import java.util.HashMap;

public interface IConditionChecker {
    boolean checkCondition(HashMap<String, Object> row);
}
