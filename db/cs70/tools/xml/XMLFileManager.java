package eg.edu.alexu.csd.oop.db.cs70.tools.xml;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import eg.edu.alexu.csd.oop.db.cs70.validators.HierarchyEditor;
import eg.edu.alexu.csd.oop.db.cs70.validators.HierarchyReader;
import eg.edu.alexu.csd.oop.db.cs70.validators.nodes.CNode;

/**
 * has all 3 operations (add, rename, drop) for all 4 levels
 * (db, t, c, r), row modification is not supported.
 * @author Walid Zattout, Ahmed Waleed
 * @category XML Utility
 * @dPattern Facade
 */
public class XMLFileManager implements FileManager {

	private DirUtil dirManager;
	private HierarchyEditor update;
	private HierarchyReader info;
	
	public XMLFileManager() {
		dirManager = new DirUtil();
		update     = new HierarchyEditor();
		info       = new HierarchyReader();
	}
	
    //adders:
    public boolean addDatabase(String path) {
        String currentDir = dirManager.getCurrentDirPath("");
        String [] splitedPath = path.split("\\\\|\\/");
        
        String databaseDir = createDirRecursive(currentDir, path);
        databaseDir += splitedPath[splitedPath.length-1];
        
        File file = new File(databaseDir);
        if(file.mkdir() || file.exists()) {
        	update.registerDatabase(path);
        	return true;
        }
        return false;
    }
	public boolean addTable(String db, String name) {
        String currentDir = dirManager.getCurrentDirPath(info.fetchDatabase(db).getDbPath()  + "\\" + name + ".xml");
        //System.out.println(currentDir);
        
        File file = new File(currentDir);
        
        boolean successfullyCreated = false;
        try {
        	successfullyCreated = file.createNewFile();
		} catch (IOException e) {}
        
        if(successfullyCreated) {
        	update.registerTable(db, name);
        }
        
        return successfullyCreated;
    }
    @SuppressWarnings("rawtypes")
	public boolean addColumn(String db, String t, String name, Class type) {
    	ArrayList<HashMap<String, Object>> nowTable = fetchTableData(info.fetchDatabase(db).getDbPath(), t);
    	
    	if(nowTable.isEmpty()) {
    		HashMap<String, Object> dummyRow = new HashMap<String, Object>();
    		dummyRow.put(name, null);
    		nowTable.add(dummyRow);
    		//System.out.println("dummy row is added");
    	}else{
    		int prevSize = nowTable.get(0).size();
	        nowTable.get(0).put(name, null);
	        int nextSize = nowTable.get(0).size();
	        
	        boolean nothingIsAdded = (prevSize == nextSize);
	        if (nothingIsAdded)
	            return false;
	        
	        for (int i = 0; i < nowTable.size(); i++){
	            nowTable.get(i).put(name, null);
	        }
    	}
    	//System.out.println(nowTable);
    	
    	update.registerColumn(db, t, name, type);
        dirManager.saveTable(info.fetchDatabase(db).getDbPath(), t, nowTable);
        return true;
    }
    public int addRow(String db, String t, HashMap<String, Object> args) {
    	ArrayList<HashMap<String, Object>> nowTable = fetchTableData(info.fetchDatabase(db).getDbPath(), t);
    	
        int prevSize = nowTable.size();
        
        String[]columns = getColumnsOfT(db, t);
        HashMap<String, Object> row = createNullRow(columns);
        row = copyRowData(args, row);
        
        if(isInvalidRow(db, t, row)) {
        	System.out.println("row is rejected");
        	return prevSize;
        }
        
        nowTable.add(prevSize, row);
        
        int nextSize = nowTable.size();
        
        if(nextSize == 2)
	        if(removeCreatedNullRow(columns, nowTable)) {
	        	//System.out.println("dummy row is deleted");
	        	nextSize = 1;
	        }
        
        dirManager.saveTable(info.fetchDatabase(db).getDbPath(), t, nowTable);
        
        boolean nothingIsAdded = (prevSize == nextSize);
        if (nothingIsAdded)
            return prevSize;
        
        return nextSize;
    }
    
    private String createDirRecursive(String currentDir, String path) {
    	
    	String [] splitedPath = path.split("\\\\|\\/");
    	
        for(int i=0; i<splitedPath.length-1; i++) {
        	//System.out.println("Created: " + splitedPath[i]);
        	currentDir += splitedPath[i] + "\\" ;
        	File file = new File(currentDir);
        	if(!file.exists())
        		file.mkdir();
        }
        
		return currentDir;
	}
	private HashMap<String, Object> createNullRow(String[] columns){
    	HashMap<String, Object> row = new HashMap<String, Object>();
    	for (int i = 0; i < columns.length; i++){
            row.put(columns[i], null);
        }
    	return row;
    }
    private boolean removeCreatedNullRow(String[] columns, ArrayList<HashMap<String, Object>> nowTable){
		boolean firstRowIsNull = true;
		for (int i = 0; i < columns.length; i++){
			if(nowTable.get(0).get(columns[i]) != null)
				firstRowIsNull = false;
		}
		if(firstRowIsNull) {
			nowTable.remove(0);
			return true;
		}
    	return false;
    }
    @SuppressWarnings("rawtypes")
	private HashMap<String, Object> copyRowData(HashMap<String, Object> from, HashMap<String, Object> to){
    	Iterator listIterator = from.entrySet().iterator();
        while(listIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)listIterator.next();
            to.put(mapElement.getKey().toString(), mapElement.getValue());
        }
        return to;
    }
    private String[] getColumnsOfT(String db, String t) {
    	CNode[] columns = info.fetchTable(db, t).getChildren();
    	String[] cArr = new String[columns.length];
    	
    	for(int i=0; i<columns.length; i++)
    		cArr[i] = columns[i].getCName();
    	
    	return cArr;
    }
    @SuppressWarnings("rawtypes")
	private boolean isInvalidRow(String db, String t, HashMap<String, Object> row) {
    	CNode[] columns = info.fetchTable(db, t).getChildren();
    	
    	Iterator listIterator = row.entrySet().iterator();
        while(listIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)listIterator.next();
            for(int i=0; i<columns.length; i++)
            	if(columns[i].getCName().equals(mapElement.getKey().toString()))
            		if(!columns[i].getCType().equals(mapElement.getValue().getClass())) {
            			/*System.out.println("rejected because of:" + columns[i].getCType() 
            							  +" does not equal " + mapElement.getValue().getClass());*/
            			return true;
            		}
            			
        }
    	
		return false;
	}
    
    //editors:
    public boolean renameDatabase(String oldName, String newName) {
        String currentDir = dirManager.getCurrentDirPath("");
        if(rename(currentDir, oldName, newName)) {
        	update.renameDatabase(oldName, newName);
        	return true;
        }
        return false;
    }
    public boolean renameTable(String db, String oldName, String newName) {
    	String currentDir = dirManager.getCurrentDirPath(info.fetchDatabase(db).getDbPath() + "\\");
    	if(rename(currentDir, oldName + ".xml", newName + ".xml")) {
        	update.renameTable(db, oldName, newName);
        	return true;
        }
        return false;
    }
    public boolean renameColumn(String db, String t, String oldName, String newName) {
    	ArrayList<HashMap<String, Object>> nowTable = fetchTableData(info.fetchDatabase(db).getDbPath(), t);
    	
    	int prevSize = nowTable.get(0).size();
    	
        for (int i = 0; i < nowTable.size(); i++){
            nowTable.get(i).put(newName, nowTable.get(i).get(oldName)) ;
            nowTable.get(i).remove(oldName);
        }
        
        int nextSize = nowTable.get(0).size();
        
        boolean successfullyRenamed = (prevSize == nextSize);
        if (successfullyRenamed) {
        	update.renameColumn(db, t, oldName, newName);
        	dirManager.saveTable(info.fetchDatabase(db).getDbPath(), t, nowTable);
            return true;
        }
        
        return false;
    }
    
    private boolean rename(String currentDir, String oldName, String newName) {
    	File sourceFile      = new File(currentDir + oldName);
        File destinationFile = new File(currentDir + newName);
        
        if (sourceFile.renameTo(destinationFile))
            return true;

        return false;
    }
    
    //droppers:
    public boolean dropDatabase(String name) {
    	String fileDir = dirManager.getCurrentDirPath(info.fetchDatabase(name).getDbPath());
    	if(deleteDirectoryRecursively(new File(fileDir))) {
    		update.unregisterDatabase(name);
    		return true;
    	}
        return false; 
    }
    public boolean dropTable(String db, String name) {
    	String fileDir = dirManager.getCurrentDirPath(info.fetchDatabase(db).getDbPath() + "\\" + name + ".xml");
        if(delete(fileDir)) {
    		update.unregisterTable(db, name);
    		return true;
    	}
        return false; 
    }
    public boolean dropColumn(String db, String t, String name) {
    	ArrayList<HashMap<String, Object>> nowTable = fetchTableData(info.fetchDatabase(db).getDbPath(), t);
    	
    	if(nowTable.isEmpty())
    		return false;
    	
    	int prevSize = nowTable.get(0).size();
    	
        for (int i = 0; i < nowTable.size(); i++){
            nowTable.get(i).remove(name);
        }
        
        int nextSize = nowTable.get(0).size();
        
        boolean nothingIsRemoved = (prevSize == nextSize);
        if (nothingIsRemoved) {
            return false;
        }
        
        update.unregisterColumn(db, t, name);
        dirManager.saveTable(info.fetchDatabase(db).getDbPath(), t, nowTable);
        return true;
    }
    
    private boolean delete(String fileDir) {
    	File fileToDelete = new File(fileDir);
        if (fileToDelete.delete())
            return true;
        return false;
    }
    private boolean deleteDirectoryRecursively(File dir) {
    	if (dir.isDirectory()) {
    		
    		File[] children = dir.listFiles();
    		
	    	for (int i = 0; i < children.length; i++) {
	    		boolean success = deleteDirectoryRecursively(children[i]);
	    		if (!success) 
	    			return false;
	    	} 
	    	
    	} // either file or an empty directory
    	
    	return dir.delete();
    	
    } // https://javarevisited.blogspot.com/2015/03/how-to-delete-directory-in-java-with-files.html

    //fetcher:
    public ArrayList<HashMap<String, Object>> fetchTableData(String dbPath, String t) {
        return dirManager.loadTable(dbPath, t.toLowerCase());
    }
    
   //setter:
    public void setTableData(String dbPath, String t, ArrayList<HashMap<String, Object>> nowTable) {
        dirManager.saveTable(dbPath, t.toLowerCase(), nowTable);
    }
    
    
}
