package eg.edu.alexu.csd.oop.db.cs70.tools.xml;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("rawtypes")
public interface FileManager {
	
    //adders:
    public boolean addDatabase(String name);
    public boolean addTable(String db, String name);
	public boolean addColumn(String db, String t, String name, Class type);
    public int addRow(String db, String t, HashMap<String, Object> args);
    
    //editors:
    public boolean renameDatabase(String oldName, String newName);
    public boolean renameTable(String db, String oldName, String newName);
    public boolean renameColumn(String db, String t, String oldName, String newName);
    
    //droppers:
    public boolean dropDatabase(String name);
    public boolean dropTable(String db, String name);
    public boolean dropColumn(String db, String t, String name);
        
    //fetcher:
    public ArrayList<HashMap<String, Object>> fetchTableData(String db, String t);
    
    //setter:
    public void setTableData(String db, String t, ArrayList<HashMap<String, Object>> nowTable);

}
