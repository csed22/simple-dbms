package eg.edu.alexu.csd.oop.db.cs70.tools.xml;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * uses beans.XMLEncoder / beans.XMLDecoder to save / load data.
 * @author Walid Zattout
 * @category XML Utility
 */
public class XMLUtil {

	 protected void saveTable(String dbPath, String t, ArrayList<HashMap<String, Object>> nowTable) throws IOException {

		String currentDir = getXMLFilePath(dbPath, t);
		
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(currentDir);
		} catch (Exception e) {
			System.out.println("The system cannot find the path specified: " + currentDir);
			return;
		}
		
		// https://howtodoinjava.com/java/serialization/xmlencoder-and-xmldecoder-example/
	    XMLEncoder encoder = new XMLEncoder(fos);
	    
	    encoder.setExceptionListener(new ExceptionListener() {
            public void exceptionThrown(Exception e) {
                e.printStackTrace();
            }
	    });
	    
	    encoder.writeObject(nowTable);
	    encoder.close();
	   
	    fos.close();

	 }

	 @SuppressWarnings("unchecked")
	 protected  ArrayList<HashMap<String, Object>> loadTable(String dbPath, String t) throws IOException {

		String currentDir = getXMLFilePath(dbPath, t);
		ArrayList<HashMap<String, Object>> nowTable;
		
		InputStream fos = null;
		XMLDecoder decoder = null;
		
		try {
			
			fos = new FileInputStream(currentDir);
			// https://howtodoinjava.com/java/serialization/xmlencoder-and-xmldecoder-example/
			decoder = new XMLDecoder(fos);
			
			decoder.setExceptionListener(new ExceptionListener() {
	            public void exceptionThrown(Exception e) {
	            	//e.printStackTrace();
	            }
		    });
			
			try {
				nowTable = (ArrayList<HashMap<String, Object>>) decoder.readObject();
			} catch (Exception e) {
				//System.out.println(".xml file is empty");
				nowTable = new ArrayList<HashMap<String, Object>>();
			}
			
			decoder.close();
			fos.close();
			
		} catch (Exception e) {
			
			System.out.println("The system cannot find the path specified: " + currentDir);
			nowTable = new ArrayList<HashMap<String, Object>>();
		}

		return nowTable;
	    
	 }
	 
	 private String getXMLFilePath(String dbPath, String t) {
		//System.out.println("Openning : " + System.getProperty("user.dir")  + "\\" + dbPath + "\\" + t + ".xml");
		return System.getProperty("user.dir")  + "\\" + dbPath + "\\" + t + ".xml";
	 }
	
}
