package eg.edu.alexu.csd.oop.db.cs70.tools.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * responsible for fetching any information about the 'data' folder.
 * @author Walid Zattout, Ahmed Waleed
 * @category Directories Utility
 */
public class DirUtil {
	
	public XMLUtil beansEncodeDecode = new XMLUtil();
	
	//getters:
    public String[] getDatabases() {
        return listDirContents("");
    }
    public String[] getTablesOfDB(String db) {
        return listDirContents(db);
    }
    public String[] getColumnsOfT(String db, String t) {
    	ArrayList<HashMap<String, Object>> nowTable = loadTable(db, t);
        return parseHashMapKeys(nowTable);
    }
    public int getRowCount(String db, String t) {
    	ArrayList<HashMap<String, Object>> nowTable = loadTable(db, t);
        return nowTable.size();
    }
    
    public String getCurrentDirPath(String dir) {
        return System.getProperty("user.dir") + "\\" + dir;
    }
    
    private String[] listDirContents(String dir) {
    	String currentDir  = getCurrentDirPath(dir);
        File directoryPath = new File(currentDir);
        return directoryPath.list();
    }
    private String[] parseHashMapKeys(ArrayList<HashMap<String, Object>> nowTable) {
    	if(nowTable.isEmpty())
    		return null;
    	int size = nowTable.get(0).size();
        String [] columns = new String[size];
        int i = 0;
        for ( String key : nowTable.get(0).keySet() ) {
            columns [i++] = key;
        }
        return columns;
    }
    
    public void saveTable(String dbPath, String t, ArrayList<HashMap<String, Object>> nowTable){
		try {
			beansEncodeDecode.saveTable(dbPath, t, nowTable);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public ArrayList<HashMap<String, Object>> loadTable(String dbPath, String t){
    	ArrayList<HashMap<String, Object>> nowTable = null;
        try {
			nowTable = beansEncodeDecode.loadTable(dbPath, t);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return nowTable;
    }

}
