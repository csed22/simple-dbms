package eg.edu.alexu.csd.oop.db.cs70;

import eg.edu.alexu.csd.oop.db.Database;
import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.facade.IFacade;
import eg.edu.alexu.csd.oop.db.cs70.parsers.*;
import eg.edu.alexu.csd.oop.db.cs70.parsers.factory.*;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

import java.sql.SQLException;

public class DatabaseImpl implements Database {

    private IParserFactory parserFactory = new ParserFactory() ;
    ParametersValidator tester = new ParametersValidator();

    public DatabaseImpl(){
    	Facade.currentDatabaseName = null;
    }

    @Override
    public String createDatabase(String databasePath, boolean dropIfExists) {
            try {
                if(dropIfExists) {
                    executeStructureQuery("DROP DATABASE " + databasePath);
                }
                executeStructureQuery("CREATE DATABASE "+ databasePath);
            } catch (SQLException e) {
                e.printStackTrace();
            }
    	//tester.print();
        return databasePath;
    }

	@Override
    public boolean executeStructureQuery(String query) throws SQLException {
        try {
            return (boolean) dryer(query, "Invalid Structure Query");
        } catch (NullPointerException e){}
    	//tester.print();
        return false;
    }

    @Override
    public Object[][] executeQuery(String query) throws SQLException {
        try {
            return (Object[][]) dryer(query, "Invalid Query");
        } catch (NullPointerException e){}
    	//tester.print();
        return new Object[0][];
    }

    @Override
    public int executeUpdateQuery(String query) throws SQLException {
        try {
            return (int) dryer(query, "Invalid Update Query");
        } catch (NullPointerException e){}
    	//tester.print();
        return 0;
    }

    public Object dryer(String query, String error) throws SQLException {
    	
    	IFacade prepare = new Facade();
    	query = prepare.setInputString(query);
    	
        IParser parser = parserFactory.getParser(query);
        parser.parse();
        if(parser.isSuccessful())
            return parser.executeCommand();
        else {
            System.out.println(error);
            throw new SQLException();
        }
    }
}
