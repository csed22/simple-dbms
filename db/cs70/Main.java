package eg.edu.alexu.csd.oop.db.cs70;

import eg.edu.alexu.csd.oop.db.cs70.config.Configurations;
import eg.edu.alexu.csd.oop.db.cs70.facade.Facade;
import eg.edu.alexu.csd.oop.db.cs70.facade.IFacade;
import eg.edu.alexu.csd.oop.db.cs70.input.InputCLI;
import eg.edu.alexu.csd.oop.db.cs70.tools.time.ExecutionTimeUtil;
import eg.edu.alexu.csd.oop.db.cs70.ui.InterfaceChooser;
import eg.edu.alexu.csd.oop.db.cs70.validators.ParametersValidator;

import java.io.File;
import java.sql.SQLException;

/**
 * Since java cannot open its own console terminal, and it is almost
 * Impossible to focus CommandLineInterface(CLI) from GraphicalUserInterface(GUI).
 * The solution is simply:
 * a program that launches what is able to start it (.bat, windowsCMD or JfameWindow ),
 * @author Ahmed Waleed
 * @category MainClass
 */
@SuppressWarnings("unused")
public class Main {

	
	public static void main(String[] args) throws SQLException {

		trueMain();
		//mainInConsole();
		
	}
	
	private static void trueMain() throws SQLException {
			
			InterfaceChooser mainInterface = new InterfaceChooser();
			
			if(mainInterface.userPreferences.isAllowedToOpen()) {
				mainInterface.act();
			} else {
				//this branch is only reachable if CLI is chosen successfully by the InterfaceChooser
		
				//at the start of the code, to allow the program to open again if an error has happened
				mainInterface.userPreferences.allow();
				mainInConsole();
	        	
			}	
			
		}
	
	private static ParametersValidator tester = new ParametersValidator();
	private static String currentdb = new String();

	private static void mainInConsole() throws SQLException {
		
		InputCLI extractInput = new InputCLI();
		Facade facade = new Facade();
		String queryText = "";
		System.out.print("> ");
		
		//the input loop
		while(true){
			
			queryText = extractInput.readScreenInput();
			
			ExecutionTimeUtil timer = new ExecutionTimeUtil();
			Facade.currentDatabaseName = currentdb;
			
			//simple dummy use command
			if(queryText.contains("USE") || queryText.contains("use")) {
				dummyUseCommand(queryText);
				System.out.println("Succeeded in: " + timer.elapsedTime()/1000.0 + " sec.");
				continue;
			}
			try {
				facade.wireAndExecute(queryText);
			} catch (Exception e) {
				//e.printStackTrace();
				if(e.getMessage()!=null)
					System.out.println(e.getMessage());
			}
			
			//tester.print();
        	System.out.println("Succeeded in: " + timer.elapsedTime()/1000.0 + " sec.");
        	
		}
		
	}
	
	private static void dummyUseCommand(String queryText) {
		
			String query = queryText.replace(";", "");
			String[] splited = query.split(" ");
			if(splited.length==2) {
				if(tester.isDatabaseHere(splited[1])) {
					currentdb = splited[1].replace(";", "");
					System.out.println("Current database is set to: " + splited[1].replace(";", ""));
				}else
					System.out.println("Couldn't change current database");
			}
			
	}

	
	
}
