package eg.edu.alexu.csd.oop.db.cs70.ui;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import eg.edu.alexu.csd.oop.db.cs70.controllers.Controller;
import eg.edu.alexu.csd.oop.db.cs70.controllers.InterfaceController;
import eg.edu.alexu.csd.oop.db.cs70.ui.strategies.BATStrategy;
import eg.edu.alexu.csd.oop.db.cs70.ui.strategies.GUIStrategy;
/**
 * A small window to let you pick which interface you prefer.
 * @author Ahmed Waleed
 * @category InterfaceChooser
 */
@SuppressWarnings("serial")
public class InterfaceChooser extends JFrame{

	public Controller userPreferences;

	private JButton cli;
	private JButton gui;
	private UserInterface startUI;
	
	public InterfaceChooser() {
		super("Interface");
		
		userPreferences = new InterfaceController();
		setLayout(new FlowLayout());
		
		// https://coderanch.com/t/333399/java/set-taskbar-icon
        try {
            setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("db-icon.png")));
            // https://icon-library.net/icon/data-analysis-icon-25.html
		} catch (Exception e) {
			//System.out.println("Coudldn't find icon: " + ClassLoader.getSystemResource("db-icon.png"));
		}
		
		// 72 x 50 pngs -> put them in bin folder not src (it won't work at least for me)
		Icon c = new ImageIcon(getClass().getResource("CMD.png"));
		Icon g = new ImageIcon(getClass().getResource("GUI.png"));
		
		cli = new JButton("CLI  ", c);
		add(cli);
		
		gui = new JButton("GUI  ",g);
		add(gui);
		
		HandlerClass handler = new HandlerClass();
		cli.addActionListener(handler);
		gui.addActionListener(handler);
	}

	private class HandlerClass implements ActionListener{

	public void actionPerformed(ActionEvent event) {
			Object source = event.getSource();
			
			 if (source == cli) {
				 startUI = new UserInterface(new BATStrategy());
				 userPreferences.deny();
			 }
			 if (source == gui) {
				 startUI = new UserInterface(new GUIStrategy());
				 userPreferences.allow();
			 }
			 
			startUI.stratUp();
			setVisible(false); // you can't see me!
			dispose(); // Destroy the JFrame object
			// https://stackoverflow.com/questions/1234912/how-to-programmatically-close-a-jframe
		}		
	}
	
	/**
	 * Displays a window to choose between CLI & GUI interface
	 */
	public void act() {

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setSize(290,72);
		this.setLocationRelativeTo(null);

		this.setUndecorated(true);
	    // https://stackoverflow.com/questions/8701716/how-to-remove-title-bar-in-jframe
		this.setShape(new RoundRectangle2D.Double(0, 0, 290, 72, 5, 5));
		// https://stackoverflow.com/questions/18935558/java-rounded-corners-on-jframe
		
		this.setVisible(true);
	}
		
}	// Java (Beginner) Tutorials : 62, 63