package eg.edu.alexu.csd.oop.db.cs70.ui.strategies;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * <<Interface>>
 * @author Ahmed Waleed
 * @category User Interface
 * @dPattern Strategy
 */
public interface UserInterfaceStrategy {
	
	public void excute() throws IOException, InterruptedException, URISyntaxException;
	
}

//Switching from GUI to Command Prompt in Java: https://stackoverflow.com/questions/350978/switching-from-gui-to-command-prompt-in-java