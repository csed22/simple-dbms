package eg.edu.alexu.csd.oop.db.cs70.ui.strategies;

import java.io.*;
import java.awt.GraphicsEnvironment;
import java.net.URISyntaxException;

import eg.edu.alexu.csd.oop.db.cs70.config.Configurations;
/**
 * reopens the program using windows CMD, (Not used but good to keep)
 * @author Ahmed Waleed
 * @category User Interface
 * @dPattern Strategy
 */
public class CMDStrategy implements UserInterfaceStrategy{
	
	/**
	 * FIRST SOLUION 1
	 * Starts windows CMD and execute the .jar file,
	 * a fine solution, but you have EXPORT the .jar
	 * after every edit ... and to name it same as
	 * the main class and but it in the bin folder,
	 * note: the jar file has to be exported automatically
	 * in order to make this solution bearable. 
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws URISyntaxException
	 */
	public void excute() throws IOException, InterruptedException, URISyntaxException {
		
		Console console = System.console();
		
        if(console == null && !GraphicsEnvironment.isHeadless()){
        	
            String filename = Configurations.getInstance().identifyJarLocation();
            Runtime.getRuntime().exec(new String[]{"cmd","/c","start","cmd","/k","java -jar \"" + filename + "\""});
            
        }else{
        	// Main.main(new String[0]);
        	// here comes the code.
        }
	}

	public void exit() {
		System.out.println("Program has ended, please type 'exit' to close the console");
	}
	
}

//Let a java application open a console terminal window: https://stackoverflow.com/questions/7704405/how-do-i-make-my-java-application-open-a-console-terminal-window
