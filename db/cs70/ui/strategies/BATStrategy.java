package eg.edu.alexu.csd.oop.db.cs70.ui.strategies;

import java.io.*;
import java.net.URISyntaxException;

import eg.edu.alexu.csd.oop.db.cs70.config.Configurations;

import java.awt.GraphicsEnvironment;
/**
 * reopens the program using .bat file (used as default).
 * @author Ahmed Waleed
 * @category User Interface
 * @dPattern Strategy
 */
public class BATStrategy implements UserInterfaceStrategy{

	/**
	 * SECOND SOLUION 2
	 * creates a launcher .bat to execute the .jar file
	 * which is really great, and it can be used as a switch
	 * to CLI mode when the user chooses GUI, as some sort of 'Rawshana'.
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws URISyntaxException
	 */
	@SuppressWarnings("resource")
	public void excute() throws IOException, InterruptedException, URISyntaxException {
		Console console = System.console();
		if(console == null && !GraphicsEnvironment.isHeadless()) {
	        String filename = Configurations.getInstance().identifyJarLocation();

	            File batch = new File("Launcher.bat");
                batch.createNewFile();
                PrintWriter writer = new PrintWriter(batch);
                
                writer.println("@echo off");
                writer.println("java -jar \"" + filename + "\"");
                /* maybe add a condition here to enable pause before exit,
                 * which prints press any key to continue (which is exit).
                 */
                writer.println("pause");
                writer.println("exit");
                writer.flush();
                
	            Runtime.getRuntime().exec("cmd /c start \"\" "+batch.getPath());

	    }else{
	        // Main.main(new String[0]);
	    	// here comes the code.
	    }
	}

}

//Let a java application open a console terminal window: https://stackoverflow.com/questions/7704405/how-do-i-make-my-java-application-open-a-console-terminal-window
