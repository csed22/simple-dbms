package eg.edu.alexu.csd.oop.db.cs70.ui.strategies;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import eg.edu.alexu.csd.oop.db.cs70.ui.InterfaceChooser;

/**
 * place holder, does not do any thing yet
 * @author Ahmed Waleed
 * @category User Interface
 * @dPattern Strategy
 */
@SuppressWarnings("serial")
public class GUIStrategy extends JFrame implements UserInterfaceStrategy{
	
	private final Color noBackground = new Color(0,0,0,0);
	
    public GUIStrategy() {
    	
        super("GUI not found!");
        
        setUndecorated(true);
        setBackground(noBackground);
        setSize(new Dimension(1100,710));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(null);
        
        // https://coderanch.com/t/333399/java/set-taskbar-icon
        try {
            setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("db-icon.png")));
            // https://icon-library.net/icon/data-analysis-icon-25.html
		} catch (Exception e) {
			//System.out.println("Coudldn't find icon: " + ClassLoader.getSystemResource("db-icon.png"));
		}
        
        ImageIcon image = new ImageIcon(getClass().getResource("space404.png"));
        // https://dribbble.com/shots/6103030-404-not-found-we-lost-404/attachments
        JLabel background = new JLabel(image);
        background.setBounds(0,0,1100,710);
        add(background);
        
        JButton goBack = new JButton("");
        goBack.addActionListener(e -> terminateAndRestart());
        goBack.setBounds(930,435,84,20);
        goBack.setBackground(noBackground);
        add(goBack);
        
    }
	
	public void excute() throws IOException, InterruptedException, URISyntaxException {
		this.setVisible(true);		
	}
	
	private void terminateAndRestart() {
		this.setVisible(false); // you can't see me!
		this.dispose(); // Destroy the JFrame object
		InterfaceChooser mainInterface = new InterfaceChooser();
		mainInterface.act();
	} // https://stackoverflow.com/questions/1234912/how-to-programmatically-close-a-jframe

}

// https://docs.oracle.com/javase/tutorial/uiswing/misc/trans_shaped_windows.html