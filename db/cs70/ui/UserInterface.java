package eg.edu.alexu.csd.oop.db.cs70.ui;

import java.io.IOException;
import java.net.URISyntaxException;

import eg.edu.alexu.csd.oop.db.cs70.ui.strategies.UserInterfaceStrategy;

/**
 * Concrete Class, user interface client.
 * @author Ahmed Waleed
 * @category User Interface
 * @dPattern Strategy
 */
public class UserInterface {

	private UserInterfaceStrategy uiStrategy;
	
	public UserInterface(UserInterfaceStrategy interfaceStrategy) {
		uiStrategy = interfaceStrategy;
	}
	
	public boolean stratUp() {
		
		try {
			uiStrategy.excute();
			return true;
		} catch (IOException | InterruptedException | URISyntaxException e) {}
		
		return false;
	
	}
	
}
