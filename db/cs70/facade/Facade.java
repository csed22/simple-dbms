package eg.edu.alexu.csd.oop.db.cs70.facade;

import java.sql.SQLException;

import eg.edu.alexu.csd.oop.db.cs70.parsers.IParser;
import eg.edu.alexu.csd.oop.db.cs70.parsers.Parser;
import eg.edu.alexu.csd.oop.db.cs70.parsers.strategies.InitialParserStrategy;

public class Facade implements IFacade {

    public static String currentDatabaseName;
    private IParser parser;
    private String inputString;
    
    @Override
    public String setInputString(String input) {
        input = input.replaceAll("<>"," neq ");
        input = input.replaceAll(">="," greq ");
        input = input.replaceAll("<="," leq ");
        input = input.replaceAll("<"," less ");
        input = input.replaceAll(">"," gre ");
        input = input.replaceAll("="," eq ");
        return input;
    }

    @Override
    public void setParser() {
        this.parser = new Parser(new InitialParserStrategy(), inputString);
    }

    @Override
    public boolean parse(String input) {
    	this.inputString  = this.setInputString(input);
        this.setParser();
        this.parser.parse();
        return this.parser.isSuccessful();
    }

    @Override
    public void wireAndExecute(String input) throws SQLException {
        if(this.parse(input))
            this.parser.executeCommand();
    }
    
}
