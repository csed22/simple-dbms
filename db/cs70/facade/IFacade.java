package eg.edu.alexu.csd.oop.db.cs70.facade;

import java.sql.SQLException;

public interface IFacade {

    String setInputString(String input);

    void setParser();

    boolean parse(String input);

    void wireAndExecute(String input) throws SQLException;
    
}
